﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Security;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System.Security.Principal;

namespace SupportLauncher2
{
    public partial class Form1 : Form
    {
        #region fields and properties
        private DataTable dtStores;
        private DataTable dtUsers;
        private Controller control = new Controller();
        #endregion

        public Form1()
        {          
            InitializeComponent();
            InitializeData();
        }

        public Form1(string param)
        {
            InitializeComponent();
            InitializeData();
        }

        internal bool IsRunAsAdmin()
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(id);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        public async void ReloadForm()
        {
            this.btnReloadData_Click(btnReloadStoreData, new EventArgs());
            //Waiting for 1st reload to have used the dataTable before it is freed up for next reload
            await Task.Delay(1000);
            this.btnReloadUserData_Click(btnReloadUserData, new EventArgs());            
        }

        //Running the csv dataload asyncron with the UI so the UI does not wait for the dataload to finish to be initialized
        private async void InitializeData()
        {            
            try
            {
                dtStores = await Task.Run(() => control.getCvsStores());                
            }
            catch (FileNotFoundException e)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this); // center message box
                MessageBox.Show("Error processing storelist:\n" + e.Message + "\n\nStore list not found or an error occured processing the list. \nPlease control configuration" +
                    " and that you have access to the storelist from your computer", "SupportLauncher - Storelist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch(IndexOutOfRangeException ei)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this); // center message box
                MessageBox.Show("Error processing storelist:\n" + ei.Message + "\n\nStore list is not correct formated. \nPlease control configuration" +
                    " and that storelist is formated correctly", "SupportLauncher - Storelist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IOException ex)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                MessageBox.Show("Error processing storelist:\n" + ex.Message + "\n\nStore list not found or an error occured processing the list. \nPlease control configuration" +
                    " and that you have access to the storelist from your computer", "SupportLauncher - Storelist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                dtUsers = await Task.Run(() => control.getCsvUsers());
            }
            catch (FileNotFoundException e)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this); // center message box
                MessageBox.Show("Error processing userlist:\n" + e.Message + "\n\nUserlist not found or an error occured processing the list. \nPlease control configuration" +
                    " and that you have access to the userlist from your computer", "SupportLauncher - Userlist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IndexOutOfRangeException ei)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this); // center message box
                MessageBox.Show("Error processing userlist:\n" + ei.Message + "\n\nUserlist is not correct formated. \nPlease control configuration" +
                    " and that userlist is formated correctly", "SupportLauncher - Userlist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IOException ex)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                MessageBox.Show("Error processing storelist:\n" + ex.Message + "\n\nStore list not found or an error occured processing the list. \nPlease control configuration" +
                    " and that you have access to the storelist from your computer", "SupportLauncher - Storelist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            await Task.Delay(2000);

            BindDataCSV(dgvStores, dtStores);
            BindDataCSV(dgvUsers, dtUsers);
        }

        //Binding a DataTable to a DataGridView
        private void BindDataCSV(DataGridView dgview, DataTable dtCsv)
        {
            bool dataLoaded = false;
            try
            {
               dgview.DataSource = dtCsv;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            try
            {
                //Hiding the coloum "_RowString" as its only used for searchstrings
                dgview.Columns["_RowString"].Visible = false;
            }
            catch (NullReferenceException)
            {

            }

            //Setting colums size due that autosize will degrease performance significantly in the form
            foreach (DataGridViewColumn column in dgview.Columns)
            {
                column.Width = 145;
            }

            if(dgview.Rows.Count > 0)
            {
                dataLoaded = true;
            }

            setLoadListInfo(dataLoaded, dgview);
            setCsvFileInfo();
        }

        //Displaying last write time for the different CSV files to a label
        private void setCsvFileInfo()
        {            
            try
            {
                lblCsvStoreFileInfo.Text = new FileInfo(control.CsvStoreFilePath).LastWriteTime.ToString();
                
            }
            catch(FileNotFoundException e)
            {
                //MessageBox.Show(e.Message);
            }
            catch (IOException ie)
            {
                //MessageBox.Show("Cannot update fileinfo due to following error: \n" + ie.Message +"\n" + control.CsvStoreFilePath, "SupportLauncher - FileInfo (Storelist)", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            try
            {
                lblCsvUserFileInfo.Text = new FileInfo(control.CsvUserFilePath).LastWriteTime.ToString();
            }
            catch (FileNotFoundException e)
            {
                //MessageBox.Show(e.Message);
            }
            catch (IOException ie)
            {
                //MessageBox.Show("Cannot update fileinfo due to following error: \n" + ie.Message + "\n" + control.CsvUserFilePath, "SupportLauncher - FileInfo (Userlist) - blablabla", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Displaying that the individual CSV file has been loaded
        private void setLoadListInfo(bool loaded, DataGridView dgView)
        {
            string dgViewName = dgView.Name;
            if (loaded)
            {
                if(dgViewName.Equals("dgvStores"))
                {
                    lblStoreListLoad.Text = Path.GetFileName(control.CsvStoreFilePath) + " loaded!";
                    lblStoreListLoad.ForeColor = Color.ForestGreen;
                    //txtStorePwD.Text = control.StorePassword;
                }
                if (dgViewName.Equals("dgvUsers"))
                {
                    lblUserListLoad.Text = Path.GetFileName(control.CsvUserFilePath) + " loaded!";
                    lblUserListLoad.ForeColor = Color.ForestGreen;
                }
            }
            else
            {
                if (dgViewName.Equals("dgvStores"))
                {
                    lblStoreListLoad.Text = Path.GetFileName(control.CsvStoreFilePath) + " NOT loaded!";
                    lblStoreListLoad.ForeColor = Color.DarkRed;
                }
                if (dgViewName.Equals("dgvUsers"))
                {
                    lblUserListLoad.Text = Path.GetFileName(control.CsvUserFilePath) + " NOT loaded!";
                    lblUserListLoad.ForeColor = Color.DarkRed;
                }
            }
        }

        private void btnParseCSV_Click(object sender, EventArgs e)
        {
            //openFileDialog1.ShowDialog();            
            //dgvStores.DataSource = null;
            //txtFilePath.Text = openFileDialog1.FileName;
            //txtFilePath.Text = @"C:\Temp\BestsellerStores.csv";
            
            //BindDataCSV(txtFilePath.Text);

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if(tabControl1.SelectedTab == tabControl1.TabPages["tabStores"])
                {
                    dtStores.DefaultView.RowFilter = string.Format("[_RowString] LIKE '%{0}%'", txtSearch.Text);
                    dtUsers.DefaultView.RowFilter = string.Empty;
                }                    
                if (tabControl1.SelectedTab == tabControl1.TabPages["tabUsers"])
                {
                    dtUsers.DefaultView.RowFilter = string.Format("[_RowString] LIKE '%{0}%'", txtSearch.Text);
                    dtStores.DefaultView.RowFilter = string.Empty;
                }                    
            }
            catch (Exception)
            {

            }            
        }

        private void dgvStores_SelectionChanged(object sender, EventArgs e)
        {            
            int storeIPRowIndex = control.StoreIPIndex;
            string storeIP;           
            if (dgvStores.CurrentRow != null)
            {
                //int rowIndex = dgvStores.CurrentRow.Index;
                //DataGridViewRow selectedStoreRow = dgvStores.Rows[rowIndex];
                //storeIP = selectedStoreRow.Cells[storeIPRowIndex].Value.ToString();
                //txtSelectedStoreIP.Text = storeIP;
                
                rdbStoreIP0.Checked = true;
                storeIP = getSelectedRowID(storeIPRowIndex);
                txtSelectedStoreIP.Text = storeIP;
                activeRowElement(storeIP);

            }
        }

        private void dgvUsers_SelectionChanged(object sender, EventArgs e)
        {
            int userBSRowIndex = control.UserBSIndex;
            string userID;
            if (dgvUsers.CurrentRow != null)
            {
                //int rowIndex = dgvUsers.CurrentRow.Index;
                //DataGridViewRow selectedUserRow = dgvUsers.Rows[rowIndex];
                //userID = selectedUserRow.Cells[userBSRowIndex].Value.ToString();
                userID = getSelectedRowID(userBSRowIndex);
                txtSelectedUserPC.Text = userID;
                activeRowElement(userID);
                //txtSelectedUserPC.Text = selectedUserRow.Cells[userBSRowIndex].Value.ToString();
            }
        }

        //Getting the store IP or user ID from the selected row in the Datagrid
        private string getSelectedRowID(int index)
        {
            string id = "";
            int rowIndex;
            DataGridViewRow selectedRow;

            try
            {
                if (tabControl1.SelectedIndex == 0)
                {                    
                    int storeIPRowIndex = control.StoreIPIndex;
                    rowIndex = dgvStores.CurrentRow.Index;
                    selectedRow = dgvStores.Rows[rowIndex];
                    //id = selectedRow.Cells[storeIPRowIndex].Value.ToString();
                    id = selectedRow.Cells[index].Value.ToString();
                }
                if (tabControl1.SelectedIndex == 1)
                {
                    int userBSRowIndex = control.UserBSIndex;
                    rowIndex = dgvUsers.CurrentRow.Index;
                    selectedRow = dgvUsers.Rows[rowIndex];
                    //id = selectedRow.Cells[userBSRowIndex].Value.ToString();
                    id = selectedRow.Cells[index].Value.ToString();
                }
            }
            catch
            {
                
            }
            return id;
        }

        //Enabling or disabling actionButtons and radiobuttons related to a store/user and updating password TextBox
        private void activeRowElement(string rowElement)
        {
            if (rowElement != "")
            {
                if(tabControl1.SelectedIndex == 0)
                {
                    pnlStoreButtons.Enabled = true;
                    pnlStoreIDs.Enabled = true;
                    txtStorePwD.Text = control.StorePassword;
                }
                if (tabControl1.SelectedIndex == 1)
                {
                    pnlUserButtons.Enabled = true;
                    pnlUserIDs.Enabled = true;
                    txtUserPwD.Text = control.UserPassword;
                }

            }
            else
            {
                if (tabControl1.SelectedIndex == 0)
                {                    
                    pnlStoreButtons.Enabled = false;
                    pnlStoreIDs.Enabled = false;
                    txtStorePwD.Clear();
                }
                if (tabControl1.SelectedIndex == 1)
                {                    
                    pnlUserButtons.Enabled = false;
                    pnlUserIDs.Enabled = false;
                    txtUserPwD.Clear();
                }

            }
        }

        private void dgvStores_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            CountRows();
        }

        private void dgvStores_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            CountRows();
        }

        private void dgvUsers_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            CountRows();
        }

        private void dgvUsers_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            CountRows();
        }

        private void CountRows()
        {
            if (tabControl1.SelectedIndex == 0)
                lblRowCountStores.Text = String.Format("Results: {0}", dgvStores.Rows.Count);
            if (tabControl1.SelectedIndex == 1)
                lblRowCountUsers.Text = String.Format("Results: {0}", dgvUsers.Rows.Count);
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabStores"])
            { 
                    if (dgvStores.CurrentCell != null || dgvStores.CurrentCell.Value != null)
                {
                    if(dgvStores.CurrentCell.RowIndex != -1 || dgvStores.CurrentCell.ColumnIndex != -1)
                    {
                        try
                        {
                            Clipboard.SetText(dgvStores.CurrentCell.Value.ToString());
                        }
                        catch (ArgumentNullException)
                        {
                        
                        }
                    }                
                }
            }
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabUsers"])
            {
                if (dgvUsers.CurrentCell != null || dgvUsers.CurrentCell.Value != null)
                {
                    if (dgvUsers.CurrentCell.RowIndex != -1 || dgvUsers.CurrentCell.ColumnIndex != -1)
                    {
                        try
                        {
                            Clipboard.SetText(dgvUsers.CurrentCell.Value.ToString());                            
                        }
                        catch (ArgumentNullException)
                        {

                        }
                    }
                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnClearSearch_Click(object sender, EventArgs e)
        {
            txtSearch.Clear();
        }

        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            txtSearch.Clear();
            CountRows();
        }

        private void radioButtons_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;
            string storeIP = getSelectedRowID(control.StoreIPIndex);
            if (storeIP != "")
            {
                string[] ipSegments = storeIP.Split('.');
                int lastIPSegment = Int32.Parse(ipSegments[ipSegments.Length - 1]);
                StringBuilder newIP = new StringBuilder();
                for (int i = 0; i < ipSegments.Length - 1; i++)
                {
                    newIP.Append(ipSegments[i]).Append(".");
                }

                if (radioButton.Checked)
                {                    
                    switch (radioButton.Text)
                    {
                        case "IP + 1":
                            lastIPSegment += 1;
                            break;
                        case "IP + 2":
                            lastIPSegment += 2;
                            break;
                        case "IP + 3":
                            lastIPSegment += 3;
                            break;
                        case "IP + 4":
                            lastIPSegment += 4;
                            break;
                        case "IP + 5":
                            lastIPSegment += 5;
                            break;
                        case "IP + 6":
                            lastIPSegment += 6;
                            break;
                        default:
                            //lastIPSegment += 0;
                            break;
                    }
                    newIP.Append(lastIPSegment);
                    txtSelectedStoreIP.Text = newIP.ToString();
                }
            }            
            /*
            var checkedButton = pnlStoreIDs.Controls.OfType<RadioButton>();
            foreach (var selectedIP in checkedButton)
                if (selectedIP is RadioButton)
                {
                    if (selectedIP.Checked)
                    {
                        //txtStorePwD.Text = selectedIP.
                        int i = checkedButton.Count();
                        txtStorePwD.Text = i.ToString();
                    }                                            
                }                    
            */
        }

        private async void btnReloadData_Click(object sender, EventArgs e)
        {                        
            lblStoreListLoad.ForeColor = Color.DarkRed;
            lblStoreListLoad.Text = "Wait! loading storelist...";
            dgvStores.DataSource = null;
            txtSearch.Clear();

            try
            {
                dtStores.Clear();
                dtStores.Rows.Clear();
                dtStores.Columns.Clear();
                txtSelectedStoreIP.Clear();
                txtStorePwD.Clear();                
                chbDisplayStorePwD.Checked = true;
            }
            catch(NullReferenceException )
            {
                
            }
            
            try
            {
                dtStores = await Task.Run(() => control.getCvsStores());
                await Task.Delay(1000);
                
            }
            catch (FileNotFoundException ex)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                MessageBox.Show("Error processing storelist:\n" + ex.Message + "\n\nStore list not found or an error occured processing the list. \nPlease control configuration" +
                    " and that you have access to the storelist from your computer", "SupportLauncher - Storelist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IndexOutOfRangeException ei)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                MessageBox.Show("Error processing storelist:\n" + ei.Message + "\n\nStore list is not correctly formated. \nPlease control configuration" +
                    " and that storelist is formated correctly", "SupportLauncher - Storelist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IOException ex)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                MessageBox.Show("Error processing storelist:\n" + ex.Message + "\n\nStore list not found or an error occured processing the list. \nPlease control configuration" +
                    " and that you have access to the storelist from your computer", "SupportLauncher - Storelist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            BindDataCSV(dgvStores, dtStores);
            activeRowElement(getSelectedRowID(control.StoreIPIndex));
        }

        private async void btnReloadUserData_Click(object sender, EventArgs e)
        {
            txtSearch.Clear();
            lblUserListLoad.ForeColor = Color.DarkRed;
            lblUserListLoad.Text = "Wait! loading userlist...";
            dgvUsers.DataSource = null;

            try
            {
                dtUsers.Clear();
                dtUsers.Rows.Clear();
                dtUsers.Columns.Clear();
                txtSelectedUserPC.Clear();
                txtUserPwD.Clear();                
                chbDisplayUserPwD.Checked = true;
            }
            catch (NullReferenceException)
            {
                
            }

            try
            {
                dtUsers = await Task.Run(() => control.getCsvUsers());
                await Task.Delay(1000);                
            }
            catch (FileNotFoundException ex)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                MessageBox.Show("Error processing userlist:\n" + ex.Message + "\n\nUserlist not found or an error occured processing the list. \nPlease control configuration" +
                    " and that you have access to the userlist from your computer", "SupportLauncher - Userlist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IndexOutOfRangeException ei)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                MessageBox.Show("Error processing userlist:\n" + ei.Message + "\n\nUserlist is not correctly formated. \nPlease control configuration" +
                    " and that userlist is formated correctly", "SupportLauncher - Userlist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (IOException ex)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                MessageBox.Show("Error processing storelist:\n" + ex.Message + "\n\nUserlist not found or an error occured processing the list. \nPlease control configuration" +
                    " and that you have access to the userlist from your computer", "SupportLauncher - Userlist", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            BindDataCSV(dgvUsers, dtUsers);
            activeRowElement(getSelectedRowID(control.UserBSIndex));
        }

        private void txtFilePath_TextChanged(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void lblSearch_Click(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void btnTeamViewerStore_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = control.TeamViewer;
            startInfo.Arguments = " -i " + txtSelectedStoreIP.Text + " --Password " + txtStorePwD.Text;
            startInfo.LoadUserProfile = true;   //Runs the external program as current logged in user
            startInfo.UseShellExecute = false;
            try
            {
                Process.Start(startInfo);
            }
            catch(Win32Exception we1)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                MessageBox.Show(we1.Message + "\n" + control.TeamViewer + "\n" + "\nPlease configure correct location of Teamviewer client"
                    , "Supportlauncher - Teamviewer", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvStores_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show("CellContent");
        }

        private void cntMenuStripCopyCell_Opening(object sender, CancelEventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void lblRowCountStores_Click(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void pnlResultStores_Paint(object sender, PaintEventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void btnVNC_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = control.VncPath;
            startInfo.Arguments = txtSelectedStoreIP.Text;
            startInfo.LoadUserProfile = true;   //Runs the external program as current logged in user
            startInfo.UseShellExecute = false;
            try
            {
                Process.Start(startInfo);
            }
            catch (Win32Exception we)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                MessageBox.Show(we.Message + "\n" + control.VncPath + "\n" + "\nPlease configure correct location of VNC Viewer"
                    , "Supportlauncher - VNC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPingPC_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "ping.exe";
            startInfo.Arguments = txtSelectedStoreIP.Text + " -t";
            try
            {
                Process.Start(startInfo);
            }
            catch
            {

            }
        }

        private void btnPingNet_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCMD_Click(object sender, EventArgs e)
        {

        }

        private void btnSQL_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = control.SqlPath;
            startInfo.Arguments = " -S " + txtSelectedStoreIP.Text + " -U sa -P " + control.SqlPw;
            startInfo.LoadUserProfile = true;   //Runs the external program as current logged in user
            startInfo.UseShellExecute = false;
            try
            {
                Process.Start(startInfo);
            }
            catch (Win32Exception we2)
            {
                MessageBoxHelper.PrepToCenterMessageBoxOnForm(this);
                MessageBox.Show(we2.Message + "\n" + control.SqlPath + "\n" + "\nPlease configure correct location of SQL Manager"
                    , "Supportlauncher - SQL Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSkype_Click(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("You are in the TabControl.SelectedIndexChanged event. tabControl1.Index is: " + tabControl1.SelectedIndex);
        }

        private void tabStores_Click(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void tabUsers_Click(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void pnlResultUsers_Paint(object sender, PaintEventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void txtSelectedUserPC_TextChanged(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void lblRowCountUsers_Click(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void dgvUsers_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void configToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*

            Form configForm = new ConfigForm(control.Config, this);
            //configForm.FormClosed += new FormClosedEventHandler(ReloadForm);
            this.Enabled = false;
            configForm.ShowDialog(this);
            */
            
            if (!IsRunAsAdmin())
            {
                // Launch itself as administrator
                ProcessStartInfo proc = new ProcessStartInfo();
                proc.UseShellExecute = true;
                proc.WorkingDirectory = Environment.CurrentDirectory;
                proc.FileName = Application.ExecutablePath;
                proc.Verb = "runas";

                try
                {
                    Process.Start(proc);
                }
                catch
                {
                    // The user refused the elevation.
                    // Do nothing and return directly ...
                    return;
                }

                Application.Exit();  // Quit itself
            }
            else
            {
                Form configForm = new ConfigForm(control.Config, this);
                //configForm.FormClosed += new FormClosedEventHandler(ReloadForm);
                this.Enabled = false;
                configForm.ShowDialog(this);
            }
            
          
        }

        //Manually loading a CSV file - Similar to the reload list function
        private async void loadCSVToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult msgResult;
            using (DialogCenteringService centeringService = new DialogCenteringService(this)) // center message box
            {
                msgResult = MessageBox.Show("Loading a csv file manually may result in inconsistency in the functionality of the program. \nProceed ? ", "WARNING!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            }

            if(msgResult == DialogResult.Yes)
            {
                string filePath;
                string fileName = "";

                if (tabControl1.SelectedIndex == 0)
                    fileName = control.CsvStoreFile;    //Setting the default filename to the store csv filename for the opendialog
                if (tabControl1.SelectedIndex == 1)
                    fileName = control.CsvUserFile;    //Setting the default filename to the user csv filename for the opendialog

                openFileDialog1.FileName = fileName;    //Setting the default filename in the opendialog to the csv filename

                DialogResult result = openFileDialog1.ShowDialog();
                if (result == DialogResult.OK)
                {
                    filePath = openFileDialog1.FileName;
                    string fileExtension = Path.GetExtension(filePath);
                    if (fileExtension.Equals(".csv"))
                    {
                        if (tabControl1.SelectedIndex == 0)
                        {
                            lblStoreListLoad.ForeColor = Color.DarkRed;
                            lblStoreListLoad.Text = "Wait! loading storelist...";
                            dgvStores.DataSource = null;
                            txtSearch.Clear();

                            try
                            {
                                dtStores.Clear();
                                dtStores.Rows.Clear();
                                dtStores.Columns.Clear();
                                txtSelectedStoreIP.Clear();
                                txtStorePwD.Clear();
                                chbDisplayStorePwD.Checked = true;
                            }
                            catch (NullReferenceException)
                            {

                            }

                            try
                            {
                                control.CsvStoreFilePath = filePath;
                                dtStores = await Task.Run(() => control.getCsvList(filePath));
                                await Task.Delay(1000);

                            }
                            catch (FileNotFoundException ex)
                            {
                                MessageBox.Show("Error processing storelist:\n" + ex.Message + "\n\nStore list not found or an error occured processing the list. \nPlease control configuration" +
                                    " and that you have access to the storelist from your computer", "SupportLauncher - Storelist", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (IndexOutOfRangeException ei)
                            {
                                MessageBox.Show("Error processing storelist:\n" + ei.Message + "\n\nStore list is not correctly formated. \nPlease control configuration" +
                                    " and that storelist is formated correctly", "SupportLauncher - Storelist", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            BindDataCSV(dgvStores, dtStores);
                            activeRowElement(getSelectedRowID(control.StoreIPIndex));

                        }
                        if (tabControl1.SelectedIndex == 1)
                        {
                            txtSearch.Clear();
                            lblUserListLoad.ForeColor = Color.DarkRed;
                            lblUserListLoad.Text = "Wait! loading userlist...";
                            dgvUsers.DataSource = null;

                            try
                            {
                                dtUsers.Clear();
                                dtUsers.Rows.Clear();
                                dtUsers.Columns.Clear();
                                txtSelectedUserPC.Clear();
                                txtUserPwD.Clear();
                                chbDisplayUserPwD.Checked = true;
                            }
                            catch (NullReferenceException)
                            {

                            }

                            try
                            {
                                control.CsvUserFilePath = filePath;
                                dtUsers = await Task.Run(() => control.getCsvUsers());
                                await Task.Delay(1000);
                            }
                            catch (FileNotFoundException ex)
                            {
                                MessageBox.Show("Error processing userlist:\n" + ex.Message + "\n\nUserlist not found or an error occured processing the list. \nPlease control configuration" +
                                    " and that you have access to the userlist from your computer", "SupportLauncher - Userlist", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            catch (IndexOutOfRangeException ei)
                            {
                                MessageBox.Show("Error processing userlist:\n" + ei.Message + "\n\nUserlist is not correctly formated. \nPlease control configuration" +
                                    " and that userlist is formated correctly", "SupportLauncher - Userlist", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            BindDataCSV(dgvUsers, dtUsers);
                            activeRowElement(getSelectedRowID(control.UserBSIndex));
                        }
                    }
                }
        
            }
                   
        }

        private void rdbStoreIP0_CheckedChanged(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void rdbStoreIP1_CheckedChanged(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void rdbStoreIP2_CheckedChanged(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void rdbStoreIP3_CheckedChanged(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void rdbStoreIP4_CheckedChanged(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void rdbStoreIP5_CheckedChanged(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void rdbStoreIP6_CheckedChanged(object sender, EventArgs e)
        {
            //NOT IMPLEMENTED
        }

        private void lblTVStorePwD_Click(object sender, EventArgs e)
        {

        }

        private void lblStoreID_Click(object sender, EventArgs e)
        {

        }

        private void chbDisplayStorePwD_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                txtStorePwD.PasswordChar = '*';
            else
                txtStorePwD.PasswordChar = '\0';
        }

        private void lblStoreListLoad_Click(object sender, EventArgs e)
        {

        }

        private void pnlResultStores_Paint_1(object sender, PaintEventArgs e)
        {

        }

        private void pnlStoreButtons_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSkypeCallStore_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = control.SkypePath;
            startInfo.Arguments = " callto:tel:" + getSelectedRowID(control.StorePhoneIndex).Replace(" ", "");
            startInfo.LoadUserProfile = true;
            startInfo.UseShellExecute = false;
            try
            {                                
                Process.Start(startInfo);                            
            }
            catch (Win32Exception we)
            {
                MessageBox.Show(we.Message + "\n" + control.SkypePath + "\n" + "\nPlease configure correct location of Microsoft Skype"
                    , "Supportlauncher - Skype", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lblCsvStoreFileInfo_Click(object sender, EventArgs e)
        {

        }

        private void lblFileUpdateStore_Click(object sender, EventArgs e)
        {

        }

        private void lblUserListLoad_Click(object sender, EventArgs e)
        {

        }

        private void lblCsvUserFileInfo_Click(object sender, EventArgs e)
        {

        }

        private void lblFileUpdateUser_Click(object sender, EventArgs e)
        {

        }

        private void btnTeamViewerUser_Click(object sender, EventArgs e)
        {
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = control.TeamViewer;
                startInfo.Arguments = " -i " + txtSelectedUserPC.Text + " --Password " + txtUserPwD.Text;
                try
                {
                    Process.Start(startInfo);
                    MessageBox.Show(startInfo.Arguments);
                }
                catch(Win32Exception we)
                {
                    MessageBox.Show(we.Message + "\n" + control.TeamViewer + "\n" + "\nPlease configure correct location of Teamviewer client"
                        , "Supportlauncher - Teamviewer", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
            }
        }

        private void btnSkypeIM_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = control.SkypePath;
            startInfo.Arguments = " im:sip:" + getSelectedRowID(control.UserSipIndex) +"@bestseller.com";
            startInfo.LoadUserProfile = true;
            startInfo.UseShellExecute = false;
            try
            {
                Process.Start(startInfo);
                //MessageBox.Show(getSelectedRowID(control.UserSipIndex).ToString());
                //MessageBox.Show(Conversions.ToString(Operators.AddObject(Operators.AddObject(control.SkypePath + " im:sip:", getSelectedRowID(control.UserSipIndex)), "@bestseller.com")));
                //Interaction.Shell(Conversions.ToString(Operators.AddObject(Operators.AddObject(control.SkypePath + " im:sip:", getSelectedRowID(control.UserSipIndex)), "@bestseller.com")), AppWinStyle.NormalFocus);
            }
            catch (Win32Exception we)
            {
                MessageBox.Show(we.Message + "\n" + control.SkypePath + "\n" + "\nPlease configure correct location of Microsoft Skype"
                    , "Supportlauncher - Skype", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCellCall_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = control.SkypePath;
            startInfo.Arguments = " callto:tel:" + getSelectedRowID(control.UserMobileIndex).Replace(" ", "");
            startInfo.LoadUserProfile = true;
            startInfo.UseShellExecute = false;
            try
            {
                Process.Start(startInfo);
                MessageBox.Show(startInfo.FileName + startInfo.Arguments);
            }
            catch (Win32Exception we)
            {
                MessageBox.Show(we.Message + "\n" + control.SkypePath + "\n" + "\nPlease configure correct location of Microsoft Skype"
                    , "Supportlauncher - Skype", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSkypeUser_Click(object sender, EventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = control.SkypePath;
            startInfo.Arguments = " callto:tel:" + getSelectedRowID(control.UserPhoneIndex).Replace(" ", "");
            startInfo.LoadUserProfile = true;
            startInfo.UseShellExecute = false;
            try
            {
                Process.Start(startInfo);
                MessageBox.Show(startInfo.FileName+startInfo.Arguments);
            }
            catch (Win32Exception we)
            {
                MessageBox.Show(we.Message + "\n" + control.SkypePath + "\n" + "\nPlease configure correct location of Microsoft Skype"
                    , "Supportlauncher - Skype", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnMapDrive_Click(object sender, EventArgs e)
        {

        }

        private void btnPingPC_Click_1(object sender, EventArgs e)
        {

        }

        private void lblTVUserPwD_Click(object sender, EventArgs e)
        {

        }

        private void chbDisplayUserPwD_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)            
                txtUserPwD.PasswordChar = '*';           
            else
                txtUserPwD.PasswordChar = '\0';
        }

        private void txtUserPwD_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblUserID_Click(object sender, EventArgs e)
        {

        }

        private void pnlUserButtons_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            CustomMsgBox.Show("This is a TEST", "MSG", "OK", "Cancel");
        }
    }
}
