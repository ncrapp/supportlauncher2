﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupportLauncher2.Properties;
using System.Configuration;
using System.Reflection;
using System.Security.Cryptography;
using System.IO;

namespace SupportLauncher2
{
    public class Config
    {
        #region fields and properties
        private Controller control;

        private string sqlPw;

        private string csvStoreFilePath;        
        private string storeDefaultPW;
        private int storeIPIndex;
        private int storePhoneIndex;        

        private string csvUserFilePath;        
        private string userDefaultPW;
        private int userBSIndex;
        private int userPhoneIndex;
        private int userMobileIndex;
        private int userSipIndex;

        private string teamViewerPath;
        private string vncPath;
        private string sqlPath;
        private string skypePath;

        private string testPath;
        private string testPath2;
        private string testPath3;
        private string testPath4;

        private string configPassword = "SecretKey";
        private byte[] _salt = Encoding.ASCII.GetBytes("0123456789abcdef");

        public string CsvStoreFilePath { get => csvStoreFilePath; set => csvStoreFilePath = value; }
        public string StoreDefaultPW { get => storeDefaultPW; set => storeDefaultPW = value; }
        public int StoreIPIndex { get => storeIPIndex; set => storeIPIndex = value; }
        public int StorePhoneIndex { get => storePhoneIndex; set => storePhoneIndex = value; }
        public string CsvUserFilePath { get => csvUserFilePath; set => csvUserFilePath = value; }
        public string UserDefaultPW { get => userDefaultPW; set => userDefaultPW = value; }
        public int UserBSIndex { get => userBSIndex; set => userBSIndex = value; }
        public int UserPhoneIndex { get => userPhoneIndex; set => userPhoneIndex = value; }
        public int UserMobileIndex { get => userMobileIndex; set => userMobileIndex = value; }
        public int UserSipIndex { get => userSipIndex; set => userSipIndex = value; }
        public string TeamViewerPath { get => teamViewerPath; set => teamViewerPath = value; }
        public string VncPath { get => vncPath; set => vncPath = value; }
        public string SqlPath { get => sqlPath; set => sqlPath = value; }
        public string SkypePath { get => skypePath; set => skypePath = value; }

        public string SqlPw { get => sqlPw; set => sqlPw = value; }

        public string TestPath { get => testPath; set => testPath = value; }
        public string TestPath2 { get => testPath2; set => testPath2 = value; }
        public string TestPath3 { get => testPath3; set => testPath3 = value; }
        public string TestPath4 { get => testPath4; set => testPath4 = value; }
        #endregion

        public Config(Controller control)
        {
            this.control = control;
            load_Settings();
            //save_Settings();
        }

        public string GetDefaultExeConfigPath(ConfigurationUserLevel userLevel)
        {
            try
            {
                var UserConfig = ConfigurationManager.OpenExeConfiguration(userLevel);
                return UserConfig.FilePath;
            }
            catch (ConfigurationException e)
            {
                return e.Filename;
            }
        }

        private void load_Settings()
        {
            //var configManager = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);  
            var configManager = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);        

            testPath = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;            
            testPath3 = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoaming).FilePath;
            //testPath2 = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location).FilePath;
            testPath2 = GetDefaultExeConfigPath(ConfigurationUserLevel.PerUserRoamingAndLocal);

            var encryptedStorePw = Settings.Default["storeDefaultPW"];
            var encryptedUserPw = Settings.Default["userDefaultPW"];
            var encryptedSqlPw = Settings.Default["SqlPw"];

            var decryptedStorePw = DecryptString(encryptedStorePw.ToString(), configPassword);
            var decryptedUserPw = DecryptString(encryptedUserPw.ToString(), configPassword);
            var decryptedSqlPw = DecryptString(encryptedSqlPw.ToString(), configPassword);

            if (File.Exists(configManager.FilePath))
            {
                //testPath4 = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;
                testPath4 = configManager.FilePath;
            }
            else
            {
                testPath4 = "NON EXISTING";
            }

            SqlPw = decryptedSqlPw;

            csvStoreFilePath = Settings.Default["CsvStoreFilePath"].ToString();
            storeDefaultPW = decryptedStorePw;
            storeIPIndex = (int)Settings.Default["storeIPIndex"];
            storePhoneIndex = (int)Settings.Default["storePhoneIndex"];

            csvUserFilePath = Settings.Default["csvUserFilePath"].ToString();
            userDefaultPW = decryptedUserPw;
            userBSIndex = (int)Settings.Default["userBSIndex"];
            userPhoneIndex = (int)Settings.Default["userPhoneIndex"];
            userMobileIndex = (int)Settings.Default["userMobileIndex"];
            userSipIndex = (int)Settings.Default["userSipIndex"];

            teamViewerPath = Settings.Default["TeamViewerPath"].ToString();
            vncPath = Settings.Default["VncPath"].ToString();
            sqlPath = Settings.Default["SqlPath"].ToString();
            skypePath = Settings.Default["SkypePath"].ToString();                   
        }

        public void save_Settings()
        {
            //var configManager = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal);
            
            Settings.Default["SqlPw"] = EncryptString(sqlPw, configPassword);

            Settings.Default["CsvStoreFilePath"] = csvStoreFilePath;
            Settings.Default["storeDefaultPW"] = EncryptString(storeDefaultPW, configPassword);
            Settings.Default["storeIPIndex"] = storeIPIndex;
            Settings.Default["storePhoneIndex"] = storePhoneIndex;

            Settings.Default["csvUserFilePath"] = csvUserFilePath;
            Settings.Default["userDefaultPW"] = EncryptString(userDefaultPW, configPassword);
            Settings.Default["userBSIndex"] = userBSIndex;
            Settings.Default["userPhoneIndex"] = userPhoneIndex;
            Settings.Default["userMobileIndex"] = userMobileIndex;
            Settings.Default["userSipIndex"] = userSipIndex;

            Settings.Default["TeamViewerPath"] = teamViewerPath;
            Settings.Default["VncPath"] = vncPath;
            Settings.Default["SqlPath"] = sqlPath;
            Settings.Default["SkypePath"] = skypePath;

            Settings.Default.Save();

            ConfigurationManager.RefreshSection("storeDefaultPW");
            ConfigurationManager.RefreshSection("userDefaultPW");
            ConfigurationManager.RefreshSection("SqlPw");

            control.ReloadData();

            
        }

        public string EncryptString(string plainText, string sharedSecret)
        {
            string result = null;
            RijndaelManaged aesAlg = null;

            try
            {
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                    msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(plainText);
                        }
                    }
                    result = Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
            finally
            {
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            return result;
        }

        public string DecryptString(string cipherText, string sharedSecret)
        {
            RijndaelManaged aesAlg = null;
            string result = null;

            try
            {
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);
                byte[] bytes = Convert.FromBase64String(cipherText);
                using (MemoryStream msDecrypt = new MemoryStream(bytes))
                {
                    aesAlg = new RijndaelManaged();
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                    aesAlg.IV = ReadByteArray(msDecrypt);
                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            result = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            finally
            {
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            return result;
        }

        private byte[] ReadByteArray(Stream s)
        {
            byte[] rawLength = new byte[sizeof(int)];
            if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
            {
                throw new SystemException("Stream did not contain properly formatted byte array");
            }

            byte[] buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
            if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
            {
                throw new SystemException("Did not read byte array properly");
            }

            return buffer;
        }
    }
}
