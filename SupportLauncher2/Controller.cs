﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupportLauncher2
{
    public class Controller
    {
        #region fields and properties
        private string csvStoreFile;
        private string csvUserFile;
        private string csvStoreFilePath;
        private string csvUserFilePath;
        private int storeIPIndex;
        private int storePhoneIndex;

        private int userBSIndex;
        private string storePassword;
        private string userPassword;
        private int userPhoneIndex;
        private int userMobileIndex;
        private int userSipIndex;

        private string teamViewerPath;
        private string vncPath;
        private string sqlPath;
        private string skypePath;
        private string sqlPw;

        private DataTable csvTable;
        private Config config;


        public string CsvStoreFilePath { get => csvStoreFilePath; set => csvStoreFilePath = value; }
        public string CsvStoreFile { get => csvStoreFile; set => csvStoreFile = value; }
        public string StorePassword { get => storePassword; set => storePassword = value; }
        public int StoreIPIndex { get => storeIPIndex; set => storeIPIndex = value; }
        public int StorePhoneIndex { get => storePhoneIndex; set => storePhoneIndex = value; }

        public string CsvUserFilePath { get => csvUserFilePath; set => csvUserFilePath = value; }
        public string CsvUserFile { get => csvUserFile; set => csvUserFile = value; }
        public int UserBSIndex { get => userBSIndex; set => userBSIndex = value; }
        public string UserPassword { get => userPassword; set => userPassword = value; }
        public int UserPhoneIndex { get => userPhoneIndex; set => userPhoneIndex = value; }
        public int UserMobileIndex { get => userMobileIndex; set => userMobileIndex = value; }
        public int UserSipIndex { get => userSipIndex; set => userSipIndex = value; }

        public string TeamViewer { get => teamViewerPath; set => teamViewerPath = value; }
        public string VncPath { get => vncPath; set => vncPath = value; }
        public string SqlPath { get => sqlPath; set => sqlPath = value; }
        public string SkypePath { get => skypePath; set => skypePath = value; }

        public string SqlPw { get => sqlPw; set => sqlPw = value; }

        public Config Config { get => config; set => config = value; }
        #endregion

        public Controller()
        {
            config = new Config(this);
            csvStoreFile = "BestsellerStores.csv";

            csvStoreFilePath = config.CsvStoreFilePath;
            storePassword = config.StoreDefaultPW;
            storeIPIndex = config.StoreIPIndex;
            storePhoneIndex = config.StorePhoneIndex;

            csvUserFile = "BestsellerUsers.csv";
            csvUserFilePath = config.CsvUserFilePath;
            userPassword = config.UserDefaultPW;
            userBSIndex = config.UserBSIndex;
            userPhoneIndex = config.UserPhoneIndex;
            userMobileIndex = config.UserMobileIndex;
            userSipIndex = config.UserSipIndex;

            teamViewerPath = config.TeamViewerPath;
            vncPath = config.VncPath;
            sqlPath = config.SqlPath;
            skypePath = config.SkypePath;

            sqlPw = config.SqlPw;
        }

        public void ReloadData()
        {
            csvStoreFile = "BestsellerStores.csv";
            csvUserFile = "BestsellerUsers.csv";
            csvStoreFilePath = config.CsvStoreFilePath;
            csvUserFilePath = config.CsvUserFilePath;
            storeIPIndex = config.StoreIPIndex;
            userBSIndex = config.UserBSIndex;
            storePassword = config.StoreDefaultPW;
            userPassword = config.UserDefaultPW;
            teamViewerPath = config.TeamViewerPath;
            vncPath = config.VncPath;
            sqlPath = config.SqlPath;
            skypePath = config.SkypePath;
            sqlPw = config.SqlPw;
        }

        public FileInfo getCsvFileInfo(string filePath)
        {
            string csvFilePath = filePath;

            FileInfo csvFileInfo = new FileInfo(csvFilePath);
            
            try
            {                
                return csvFileInfo;
            }
            
            catch (FileNotFoundException)
            {
                //throw new FileNotFoundException(filePath);
                return csvFileInfo;
            }
            
            catch (IOException)
            {
                //throw new IOException(filePath);
                return csvFileInfo;
            }                                     
        }

        public DataTable getCvsStores()
        {
            return BuildDataCSV(CsvStoreFilePath);
        }

        public DataTable getCsvUsers()
        {
            return BuildDataCSV(CsvUserFilePath);
        }

        public DataTable getCsvList(string filePath)
        {
            return BuildDataCSV(filePath);
        }

        //Building the datatable from csv file
        private DataTable BuildDataCSV(string filePath)
        {
            csvTable = new DataTable();

            string[] lines = new string[0];
           
            try
            {
                lines = File.ReadAllLines(filePath);                
            }
            catch (FileNotFoundException)
            {
                throw new FileNotFoundException(filePath); //Throwing exception to be catch in UI
            }

            catch (IOException)
            {
                throw new IOException(filePath);
            }

            if (lines.Length > 0)
            {
                //first line to create header
                string firstLine = lines[0];

                string[] headerLabels = firstLine.Replace("\"", "").Split(';');

                foreach (string headerWord in headerLabels)
                {
                    string header = headerWord;
                    csvTable.Columns.Add(new DataColumn(header));
                }

                //for data                

                for (int i = 1; i < lines.Length; i++)
                {
                    DataRow dr = csvTable.NewRow();

                    string[] dataWords = lines[i].Replace("\"", "").Split(';');
                    int coloumnIndex = 0;

                    try
                    {
                        foreach (string headerWord in headerLabels)
                        {
                            dr[headerWord] = dataWords[coloumnIndex++];
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new IndexOutOfRangeException(filePath);
                    }



                    try
                    {
                        csvTable.Rows.Add(dr);
                    }
                    catch (NullReferenceException)
                    {
                        Environment.Exit(1);
                    }
                }
            }

            if (csvTable.Rows.Count > 0)
            {
                //Adding a Coloumn "_RowString" for easy search options
                DataColumn dcRowString = csvTable.Columns.Add("_RowString", typeof(string));
                foreach (DataRow dataRow in csvTable.Rows)
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < csvTable.Columns.Count - 1; i++)
                    {
                        sb.Append(dataRow[i].ToString());
                        sb.Append("\t");
                    }
                    dataRow[dcRowString] = sb.ToString();
                }
            }
            return csvTable;
        }

        public void clearDataTable()
        {
            csvTable.Rows.Clear();
            csvTable.Columns.Clear();
        }
    }
}
