﻿using System.Windows.Forms;

namespace SupportLauncher2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.btnTeamViewerStore = new System.Windows.Forms.Button();
            this.dgvStores = new System.Windows.Forms.DataGridView();
            this.cntMenuStripCopyCell = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSelectedStoreIP = new System.Windows.Forms.TextBox();
            this.lblRowCountStores = new System.Windows.Forms.Label();
            this.pnlStoreIDs = new System.Windows.Forms.Panel();
            this.lblTVStorePwD = new System.Windows.Forms.Label();
            this.lblStoreID = new System.Windows.Forms.Label();
            this.chbDisplayStorePwD = new System.Windows.Forms.CheckBox();
            this.txtStorePwD = new System.Windows.Forms.TextBox();
            this.rdbStoreIP6 = new System.Windows.Forms.RadioButton();
            this.rdbStoreIP5 = new System.Windows.Forms.RadioButton();
            this.rdbStoreIP4 = new System.Windows.Forms.RadioButton();
            this.rdbStoreIP3 = new System.Windows.Forms.RadioButton();
            this.rdbStoreIP2 = new System.Windows.Forms.RadioButton();
            this.rdbStoreIP1 = new System.Windows.Forms.RadioButton();
            this.rdbStoreIP0 = new System.Windows.Forms.RadioButton();
            this.btnVNC = new System.Windows.Forms.Button();
            this.btnPingStore = new System.Windows.Forms.Button();
            this.btnPingNet = new System.Windows.Forms.Button();
            this.btnCMD = new System.Windows.Forms.Button();
            this.btnSQL = new System.Windows.Forms.Button();
            this.btnReloadStoreData = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabStores = new System.Windows.Forms.TabPage();
            this.lblStoreListLoad = new System.Windows.Forms.Label();
            this.pnlResultStores = new System.Windows.Forms.Panel();
            this.pnlStoreButtons = new System.Windows.Forms.Panel();
            this.btnSkypeCallStore = new System.Windows.Forms.Button();
            this.lblCsvStoreFileInfo = new System.Windows.Forms.Label();
            this.lblFileUpdateStore = new System.Windows.Forms.Label();
            this.tabUsers = new System.Windows.Forms.TabPage();
            this.pnlResultUsers = new System.Windows.Forms.Panel();
            this.lblRowCountUsers = new System.Windows.Forms.Label();
            this.lblUserListLoad = new System.Windows.Forms.Label();
            this.lblCsvUserFileInfo = new System.Windows.Forms.Label();
            this.lblFileUpdateUser = new System.Windows.Forms.Label();
            this.btnReloadUserData = new System.Windows.Forms.Button();
            this.pnlUserIDs = new System.Windows.Forms.Panel();
            this.lblTVUserPwD = new System.Windows.Forms.Label();
            this.chbDisplayUserPwD = new System.Windows.Forms.CheckBox();
            this.txtUserPwD = new System.Windows.Forms.TextBox();
            this.lblUserID = new System.Windows.Forms.Label();
            this.txtSelectedUserPC = new System.Windows.Forms.TextBox();
            this.dgvUsers = new System.Windows.Forms.DataGridView();
            this.pnlUserButtons = new System.Windows.Forms.Panel();
            this.btnTeamViewerUser = new System.Windows.Forms.Button();
            this.btnPingPC = new System.Windows.Forms.Button();
            this.btnMapDrive = new System.Windows.Forms.Button();
            this.btnSkypeIM = new System.Windows.Forms.Button();
            this.btnSkypeUser = new System.Windows.Forms.Button();
            this.btnCellCall = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClearSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStores)).BeginInit();
            this.cntMenuStripCopyCell.SuspendLayout();
            this.pnlStoreIDs.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabStores.SuspendLayout();
            this.pnlResultStores.SuspendLayout();
            this.pnlStoreButtons.SuspendLayout();
            this.tabUsers.SuspendLayout();
            this.pnlResultUsers.SuspendLayout();
            this.pnlUserIDs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsers)).BeginInit();
            this.pnlUserButtons.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "BestsellerStores.csv";
            this.openFileDialog1.Filter = "CSV Files (*.csv)|*.csv|All files (*.*)|*.*";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Location = new System.Drawing.Point(776, 31);
            this.txtSearch.Margin = new System.Windows.Forms.Padding(2);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(180, 21);
            this.txtSearch.TabIndex = 3;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // lblSearch
            // 
            this.lblSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblSearch.AutoSize = true;
            this.lblSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblSearch.Location = new System.Drawing.Point(680, 35);
            this.lblSearch.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(77, 13);
            this.lblSearch.TabIndex = 4;
            this.lblSearch.Text = "Quick Seearch";
            this.lblSearch.Click += new System.EventHandler(this.lblSearch_Click);
            // 
            // btnTeamViewerStore
            // 
            this.btnTeamViewerStore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTeamViewerStore.Location = new System.Drawing.Point(644, 11);
            this.btnTeamViewerStore.Margin = new System.Windows.Forms.Padding(2);
            this.btnTeamViewerStore.Name = "btnTeamViewerStore";
            this.btnTeamViewerStore.Size = new System.Drawing.Size(98, 41);
            this.btnTeamViewerStore.TabIndex = 7;
            this.btnTeamViewerStore.Text = "TeamViewer";
            this.btnTeamViewerStore.UseVisualStyleBackColor = true;
            this.btnTeamViewerStore.Click += new System.EventHandler(this.btnTeamViewerStore_Click);
            // 
            // dgvStores
            // 
            this.dgvStores.AllowUserToAddRows = false;
            this.dgvStores.AllowUserToDeleteRows = false;
            this.dgvStores.AllowUserToOrderColumns = true;
            this.dgvStores.AllowUserToResizeRows = false;
            this.dgvStores.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvStores.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvStores.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dgvStores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStores.ContextMenuStrip = this.cntMenuStripCopyCell;
            this.dgvStores.Location = new System.Drawing.Point(2, 2);
            this.dgvStores.Margin = new System.Windows.Forms.Padding(2);
            this.dgvStores.MultiSelect = false;
            this.dgvStores.Name = "dgvStores";
            this.dgvStores.ReadOnly = true;
            this.dgvStores.RowHeadersVisible = false;
            this.dgvStores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStores.Size = new System.Drawing.Size(1005, 466);
            this.dgvStores.TabIndex = 0;
            this.dgvStores.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStores_CellContentClick);
            this.dgvStores.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvStores_RowsAdded);
            this.dgvStores.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvStores_RowsRemoved);
            this.dgvStores.SelectionChanged += new System.EventHandler(this.dgvStores_SelectionChanged);
            // 
            // cntMenuStripCopyCell
            // 
            this.cntMenuStripCopyCell.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem});
            this.cntMenuStripCopyCell.Name = "contextMenuStrip1";
            this.cntMenuStripCopyCell.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.cntMenuStripCopyCell.Size = new System.Drawing.Size(103, 26);
            this.cntMenuStripCopyCell.Opening += new System.ComponentModel.CancelEventHandler(this.cntMenuStripCopyCell_Opening);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // txtSelectedStoreIP
            // 
            this.txtSelectedStoreIP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSelectedStoreIP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSelectedStoreIP.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtSelectedStoreIP.Location = new System.Drawing.Point(693, 4);
            this.txtSelectedStoreIP.Margin = new System.Windows.Forms.Padding(2);
            this.txtSelectedStoreIP.Name = "txtSelectedStoreIP";
            this.txtSelectedStoreIP.Size = new System.Drawing.Size(70, 14);
            this.txtSelectedStoreIP.TabIndex = 8;
            // 
            // lblRowCountStores
            // 
            this.lblRowCountStores.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblRowCountStores.AutoSize = true;
            this.lblRowCountStores.ForeColor = System.Drawing.Color.White;
            this.lblRowCountStores.Location = new System.Drawing.Point(2, 4);
            this.lblRowCountStores.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRowCountStores.Name = "lblRowCountStores";
            this.lblRowCountStores.Size = new System.Drawing.Size(49, 13);
            this.lblRowCountStores.TabIndex = 9;
            this.lblRowCountStores.Tag = "TEST";
            this.lblRowCountStores.Text = "Results: ";
            this.lblRowCountStores.Click += new System.EventHandler(this.lblRowCountStores_Click);
            // 
            // pnlStoreIDs
            // 
            this.pnlStoreIDs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlStoreIDs.BackColor = System.Drawing.Color.SlateGray;
            this.pnlStoreIDs.Controls.Add(this.lblTVStorePwD);
            this.pnlStoreIDs.Controls.Add(this.lblStoreID);
            this.pnlStoreIDs.Controls.Add(this.chbDisplayStorePwD);
            this.pnlStoreIDs.Controls.Add(this.txtStorePwD);
            this.pnlStoreIDs.Controls.Add(this.rdbStoreIP6);
            this.pnlStoreIDs.Controls.Add(this.rdbStoreIP5);
            this.pnlStoreIDs.Controls.Add(this.rdbStoreIP4);
            this.pnlStoreIDs.Controls.Add(this.rdbStoreIP3);
            this.pnlStoreIDs.Controls.Add(this.rdbStoreIP2);
            this.pnlStoreIDs.Controls.Add(this.rdbStoreIP1);
            this.pnlStoreIDs.Controls.Add(this.rdbStoreIP0);
            this.pnlStoreIDs.Controls.Add(this.txtSelectedStoreIP);
            this.pnlStoreIDs.Enabled = false;
            this.pnlStoreIDs.Location = new System.Drawing.Point(82, 468);
            this.pnlStoreIDs.Margin = new System.Windows.Forms.Padding(2);
            this.pnlStoreIDs.Name = "pnlStoreIDs";
            this.pnlStoreIDs.Size = new System.Drawing.Size(925, 22);
            this.pnlStoreIDs.TabIndex = 10;
            this.pnlStoreIDs.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlResultStores_Paint);
            // 
            // lblTVStorePwD
            // 
            this.lblTVStorePwD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTVStorePwD.AutoSize = true;
            this.lblTVStorePwD.ForeColor = System.Drawing.Color.White;
            this.lblTVStorePwD.Location = new System.Drawing.Point(767, 4);
            this.lblTVStorePwD.Name = "lblTVStorePwD";
            this.lblTVStorePwD.Size = new System.Drawing.Size(60, 13);
            this.lblTVStorePwD.TabIndex = 20;
            this.lblTVStorePwD.Text = "Password:";
            this.lblTVStorePwD.Click += new System.EventHandler(this.lblTVStorePwD_Click);
            // 
            // lblStoreID
            // 
            this.lblStoreID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblStoreID.AutoSize = true;
            this.lblStoreID.ForeColor = System.Drawing.Color.White;
            this.lblStoreID.Location = new System.Drawing.Point(671, 4);
            this.lblStoreID.Name = "lblStoreID";
            this.lblStoreID.Size = new System.Drawing.Size(19, 13);
            this.lblStoreID.TabIndex = 19;
            this.lblStoreID.Text = "ID:";
            this.lblStoreID.Click += new System.EventHandler(this.lblStoreID_Click);
            // 
            // chbDisplayStorePwD
            // 
            this.chbDisplayStorePwD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chbDisplayStorePwD.AutoSize = true;
            this.chbDisplayStorePwD.Checked = true;
            this.chbDisplayStorePwD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbDisplayStorePwD.Location = new System.Drawing.Point(907, 5);
            this.chbDisplayStorePwD.Name = "chbDisplayStorePwD";
            this.chbDisplayStorePwD.Size = new System.Drawing.Size(15, 14);
            this.chbDisplayStorePwD.TabIndex = 18;
            this.chbDisplayStorePwD.UseVisualStyleBackColor = true;
            this.chbDisplayStorePwD.CheckedChanged += new System.EventHandler(this.chbDisplayStorePwD_CheckedChanged);
            // 
            // txtStorePwD
            // 
            this.txtStorePwD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStorePwD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtStorePwD.Location = new System.Drawing.Point(830, 4);
            this.txtStorePwD.Margin = new System.Windows.Forms.Padding(2);
            this.txtStorePwD.Name = "txtStorePwD";
            this.txtStorePwD.PasswordChar = '*';
            this.txtStorePwD.Size = new System.Drawing.Size(70, 14);
            this.txtStorePwD.TabIndex = 17;
            // 
            // rdbStoreIP6
            // 
            this.rdbStoreIP6.AutoSize = true;
            this.rdbStoreIP6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdbStoreIP6.ForeColor = System.Drawing.Color.White;
            this.rdbStoreIP6.Location = new System.Drawing.Point(361, 3);
            this.rdbStoreIP6.Margin = new System.Windows.Forms.Padding(2);
            this.rdbStoreIP6.Name = "rdbStoreIP6";
            this.rdbStoreIP6.Size = new System.Drawing.Size(50, 17);
            this.rdbStoreIP6.TabIndex = 16;
            this.rdbStoreIP6.Text = "IP + 6";
            this.rdbStoreIP6.UseVisualStyleBackColor = true;
            this.rdbStoreIP6.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // rdbStoreIP5
            // 
            this.rdbStoreIP5.AutoSize = true;
            this.rdbStoreIP5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdbStoreIP5.ForeColor = System.Drawing.Color.White;
            this.rdbStoreIP5.Location = new System.Drawing.Point(302, 3);
            this.rdbStoreIP5.Margin = new System.Windows.Forms.Padding(2);
            this.rdbStoreIP5.Name = "rdbStoreIP5";
            this.rdbStoreIP5.Size = new System.Drawing.Size(50, 17);
            this.rdbStoreIP5.TabIndex = 15;
            this.rdbStoreIP5.Text = "IP + 5";
            this.rdbStoreIP5.UseVisualStyleBackColor = true;
            this.rdbStoreIP5.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // rdbStoreIP4
            // 
            this.rdbStoreIP4.AutoSize = true;
            this.rdbStoreIP4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdbStoreIP4.ForeColor = System.Drawing.Color.White;
            this.rdbStoreIP4.Location = new System.Drawing.Point(242, 3);
            this.rdbStoreIP4.Margin = new System.Windows.Forms.Padding(2);
            this.rdbStoreIP4.Name = "rdbStoreIP4";
            this.rdbStoreIP4.Size = new System.Drawing.Size(50, 17);
            this.rdbStoreIP4.TabIndex = 14;
            this.rdbStoreIP4.Text = "IP + 4";
            this.rdbStoreIP4.UseVisualStyleBackColor = true;
            this.rdbStoreIP4.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // rdbStoreIP3
            // 
            this.rdbStoreIP3.AutoSize = true;
            this.rdbStoreIP3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdbStoreIP3.ForeColor = System.Drawing.Color.White;
            this.rdbStoreIP3.Location = new System.Drawing.Point(184, 3);
            this.rdbStoreIP3.Margin = new System.Windows.Forms.Padding(2);
            this.rdbStoreIP3.Name = "rdbStoreIP3";
            this.rdbStoreIP3.Size = new System.Drawing.Size(50, 17);
            this.rdbStoreIP3.TabIndex = 13;
            this.rdbStoreIP3.Text = "IP + 3";
            this.rdbStoreIP3.UseVisualStyleBackColor = true;
            this.rdbStoreIP3.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // rdbStoreIP2
            // 
            this.rdbStoreIP2.AutoSize = true;
            this.rdbStoreIP2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdbStoreIP2.ForeColor = System.Drawing.Color.White;
            this.rdbStoreIP2.Location = new System.Drawing.Point(125, 3);
            this.rdbStoreIP2.Margin = new System.Windows.Forms.Padding(2);
            this.rdbStoreIP2.Name = "rdbStoreIP2";
            this.rdbStoreIP2.Size = new System.Drawing.Size(50, 17);
            this.rdbStoreIP2.TabIndex = 12;
            this.rdbStoreIP2.Text = "IP + 2";
            this.rdbStoreIP2.UseVisualStyleBackColor = true;
            this.rdbStoreIP2.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // rdbStoreIP1
            // 
            this.rdbStoreIP1.AutoSize = true;
            this.rdbStoreIP1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdbStoreIP1.ForeColor = System.Drawing.Color.White;
            this.rdbStoreIP1.Location = new System.Drawing.Point(68, 3);
            this.rdbStoreIP1.Margin = new System.Windows.Forms.Padding(2);
            this.rdbStoreIP1.Name = "rdbStoreIP1";
            this.rdbStoreIP1.Size = new System.Drawing.Size(48, 17);
            this.rdbStoreIP1.TabIndex = 11;
            this.rdbStoreIP1.Text = "IP + 1";
            this.rdbStoreIP1.UseVisualStyleBackColor = true;
            this.rdbStoreIP1.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // rdbStoreIP0
            // 
            this.rdbStoreIP0.AutoSize = true;
            this.rdbStoreIP0.Checked = true;
            this.rdbStoreIP0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rdbStoreIP0.ForeColor = System.Drawing.Color.White;
            this.rdbStoreIP0.Location = new System.Drawing.Point(25, 3);
            this.rdbStoreIP0.Margin = new System.Windows.Forms.Padding(2);
            this.rdbStoreIP0.Name = "rdbStoreIP0";
            this.rdbStoreIP0.Size = new System.Drawing.Size(34, 17);
            this.rdbStoreIP0.TabIndex = 10;
            this.rdbStoreIP0.TabStop = true;
            this.rdbStoreIP0.Text = "IP";
            this.rdbStoreIP0.UseVisualStyleBackColor = true;
            this.rdbStoreIP0.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // btnVNC
            // 
            this.btnVNC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVNC.Location = new System.Drawing.Point(539, 11);
            this.btnVNC.Margin = new System.Windows.Forms.Padding(2);
            this.btnVNC.Name = "btnVNC";
            this.btnVNC.Size = new System.Drawing.Size(98, 41);
            this.btnVNC.TabIndex = 11;
            this.btnVNC.Text = "VNC";
            this.btnVNC.UseVisualStyleBackColor = true;
            this.btnVNC.Click += new System.EventHandler(this.btnVNC_Click);
            // 
            // btnPingStore
            // 
            this.btnPingStore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPingStore.Location = new System.Drawing.Point(434, 11);
            this.btnPingStore.Margin = new System.Windows.Forms.Padding(2);
            this.btnPingStore.Name = "btnPingStore";
            this.btnPingStore.Size = new System.Drawing.Size(98, 41);
            this.btnPingStore.TabIndex = 12;
            this.btnPingStore.Text = "Ping PC";
            this.btnPingStore.UseVisualStyleBackColor = true;
            this.btnPingStore.Click += new System.EventHandler(this.btnPingPC_Click);
            // 
            // btnPingNet
            // 
            this.btnPingNet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPingNet.Location = new System.Drawing.Point(329, 11);
            this.btnPingNet.Margin = new System.Windows.Forms.Padding(2);
            this.btnPingNet.Name = "btnPingNet";
            this.btnPingNet.Size = new System.Drawing.Size(98, 41);
            this.btnPingNet.TabIndex = 13;
            this.btnPingNet.Text = "Ping Net";
            this.btnPingNet.UseVisualStyleBackColor = true;
            this.btnPingNet.Click += new System.EventHandler(this.btnPingNet_Click);
            // 
            // btnCMD
            // 
            this.btnCMD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCMD.Location = new System.Drawing.Point(224, 11);
            this.btnCMD.Margin = new System.Windows.Forms.Padding(2);
            this.btnCMD.Name = "btnCMD";
            this.btnCMD.Size = new System.Drawing.Size(98, 41);
            this.btnCMD.TabIndex = 14;
            this.btnCMD.Text = "CMD Telnet";
            this.btnCMD.UseVisualStyleBackColor = true;
            this.btnCMD.Click += new System.EventHandler(this.btnCMD_Click);
            // 
            // btnSQL
            // 
            this.btnSQL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSQL.Location = new System.Drawing.Point(119, 11);
            this.btnSQL.Margin = new System.Windows.Forms.Padding(2);
            this.btnSQL.Name = "btnSQL";
            this.btnSQL.Size = new System.Drawing.Size(98, 41);
            this.btnSQL.TabIndex = 15;
            this.btnSQL.Text = "SQL Server";
            this.btnSQL.UseVisualStyleBackColor = true;
            this.btnSQL.Click += new System.EventHandler(this.btnSQL_Click);
            // 
            // btnReloadStoreData
            // 
            this.btnReloadStoreData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReloadStoreData.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnReloadStoreData.FlatAppearance.BorderSize = 0;
            this.btnReloadStoreData.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReloadStoreData.ForeColor = System.Drawing.Color.White;
            this.btnReloadStoreData.Location = new System.Drawing.Point(1, 496);
            this.btnReloadStoreData.Margin = new System.Windows.Forms.Padding(2);
            this.btnReloadStoreData.Name = "btnReloadStoreData";
            this.btnReloadStoreData.Size = new System.Drawing.Size(74, 60);
            this.btnReloadStoreData.TabIndex = 17;
            this.btnReloadStoreData.Text = "Reload";
            this.btnReloadStoreData.UseVisualStyleBackColor = false;
            this.btnReloadStoreData.Click += new System.EventHandler(this.btnReloadData_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabStores);
            this.tabControl1.Controls.Add(this.tabUsers);
            this.tabControl1.ItemSize = new System.Drawing.Size(60, 18);
            this.tabControl1.Location = new System.Drawing.Point(0, 54);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(1015, 590);
            this.tabControl1.TabIndex = 18;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            // 
            // tabStores
            // 
            this.tabStores.Controls.Add(this.lblStoreListLoad);
            this.tabStores.Controls.Add(this.pnlResultStores);
            this.tabStores.Controls.Add(this.pnlStoreButtons);
            this.tabStores.Controls.Add(this.lblCsvStoreFileInfo);
            this.tabStores.Controls.Add(this.lblFileUpdateStore);
            this.tabStores.Controls.Add(this.pnlStoreIDs);
            this.tabStores.Controls.Add(this.dgvStores);
            this.tabStores.Controls.Add(this.btnReloadStoreData);
            this.tabStores.Location = new System.Drawing.Point(4, 22);
            this.tabStores.Margin = new System.Windows.Forms.Padding(2);
            this.tabStores.Name = "tabStores";
            this.tabStores.Padding = new System.Windows.Forms.Padding(2);
            this.tabStores.Size = new System.Drawing.Size(1007, 564);
            this.tabStores.TabIndex = 0;
            this.tabStores.Text = "IT - Stores";
            this.tabStores.UseVisualStyleBackColor = true;
            this.tabStores.Click += new System.EventHandler(this.tabStores_Click);
            // 
            // lblStoreListLoad
            // 
            this.lblStoreListLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblStoreListLoad.AutoSize = true;
            this.lblStoreListLoad.Font = new System.Drawing.Font("Bahnschrift SemiBold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStoreListLoad.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblStoreListLoad.Location = new System.Drawing.Point(79, 498);
            this.lblStoreListLoad.Name = "lblStoreListLoad";
            this.lblStoreListLoad.Size = new System.Drawing.Size(109, 13);
            this.lblStoreListLoad.TabIndex = 21;
            this.lblStoreListLoad.Text = "initializing storelist...";
            this.lblStoreListLoad.Click += new System.EventHandler(this.lblStoreListLoad_Click);
            // 
            // pnlResultStores
            // 
            this.pnlResultStores.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlResultStores.BackColor = System.Drawing.Color.SlateGray;
            this.pnlResultStores.Controls.Add(this.lblRowCountStores);
            this.pnlResultStores.Location = new System.Drawing.Point(2, 468);
            this.pnlResultStores.Margin = new System.Windows.Forms.Padding(2);
            this.pnlResultStores.Name = "pnlResultStores";
            this.pnlResultStores.Size = new System.Drawing.Size(80, 22);
            this.pnlResultStores.TabIndex = 17;
            this.pnlResultStores.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlResultStores_Paint_1);
            // 
            // pnlStoreButtons
            // 
            this.pnlStoreButtons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlStoreButtons.Controls.Add(this.btnSkypeCallStore);
            this.pnlStoreButtons.Controls.Add(this.btnSQL);
            this.pnlStoreButtons.Controls.Add(this.btnCMD);
            this.pnlStoreButtons.Controls.Add(this.btnPingNet);
            this.pnlStoreButtons.Controls.Add(this.btnTeamViewerStore);
            this.pnlStoreButtons.Controls.Add(this.btnPingStore);
            this.pnlStoreButtons.Controls.Add(this.btnVNC);
            this.pnlStoreButtons.Enabled = false;
            this.pnlStoreButtons.Location = new System.Drawing.Point(256, 493);
            this.pnlStoreButtons.Margin = new System.Windows.Forms.Padding(2);
            this.pnlStoreButtons.Name = "pnlStoreButtons";
            this.pnlStoreButtons.Size = new System.Drawing.Size(750, 65);
            this.pnlStoreButtons.TabIndex = 20;
            this.pnlStoreButtons.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlStoreButtons_Paint);
            // 
            // btnSkypeCallStore
            // 
            this.btnSkypeCallStore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSkypeCallStore.Location = new System.Drawing.Point(14, 11);
            this.btnSkypeCallStore.Margin = new System.Windows.Forms.Padding(2);
            this.btnSkypeCallStore.Name = "btnSkypeCallStore";
            this.btnSkypeCallStore.Size = new System.Drawing.Size(98, 41);
            this.btnSkypeCallStore.TabIndex = 16;
            this.btnSkypeCallStore.Text = "Skype Call";
            this.btnSkypeCallStore.UseVisualStyleBackColor = true;
            this.btnSkypeCallStore.Click += new System.EventHandler(this.btnSkypeCallStore_Click);
            // 
            // lblCsvStoreFileInfo
            // 
            this.lblCsvStoreFileInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCsvStoreFileInfo.AutoSize = true;
            this.lblCsvStoreFileInfo.Location = new System.Drawing.Point(79, 542);
            this.lblCsvStoreFileInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCsvStoreFileInfo.Name = "lblCsvStoreFileInfo";
            this.lblCsvStoreFileInfo.Size = new System.Drawing.Size(57, 13);
            this.lblCsvStoreFileInfo.TabIndex = 19;
            this.lblCsvStoreFileInfo.Text = "01/01/2001";
            this.lblCsvStoreFileInfo.Click += new System.EventHandler(this.lblCsvStoreFileInfo_Click);
            // 
            // lblFileUpdateStore
            // 
            this.lblFileUpdateStore.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFileUpdateStore.AutoSize = true;
            this.lblFileUpdateStore.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileUpdateStore.Location = new System.Drawing.Point(79, 525);
            this.lblFileUpdateStore.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFileUpdateStore.Name = "lblFileUpdateStore";
            this.lblFileUpdateStore.Size = new System.Drawing.Size(87, 13);
            this.lblFileUpdateStore.TabIndex = 18;
            this.lblFileUpdateStore.Text = "Last updated on:";
            this.lblFileUpdateStore.Click += new System.EventHandler(this.lblFileUpdateStore_Click);
            // 
            // tabUsers
            // 
            this.tabUsers.Controls.Add(this.pnlResultUsers);
            this.tabUsers.Controls.Add(this.lblUserListLoad);
            this.tabUsers.Controls.Add(this.lblCsvUserFileInfo);
            this.tabUsers.Controls.Add(this.lblFileUpdateUser);
            this.tabUsers.Controls.Add(this.btnReloadUserData);
            this.tabUsers.Controls.Add(this.pnlUserIDs);
            this.tabUsers.Controls.Add(this.dgvUsers);
            this.tabUsers.Controls.Add(this.pnlUserButtons);
            this.tabUsers.Location = new System.Drawing.Point(4, 22);
            this.tabUsers.Margin = new System.Windows.Forms.Padding(2);
            this.tabUsers.Name = "tabUsers";
            this.tabUsers.Padding = new System.Windows.Forms.Padding(2);
            this.tabUsers.Size = new System.Drawing.Size(1007, 564);
            this.tabUsers.TabIndex = 1;
            this.tabUsers.Text = "IT - Users";
            this.tabUsers.UseVisualStyleBackColor = true;
            this.tabUsers.Click += new System.EventHandler(this.tabUsers_Click);
            // 
            // pnlResultUsers
            // 
            this.pnlResultUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlResultUsers.BackColor = System.Drawing.Color.SlateGray;
            this.pnlResultUsers.Controls.Add(this.lblRowCountUsers);
            this.pnlResultUsers.Location = new System.Drawing.Point(2, 468);
            this.pnlResultUsers.Margin = new System.Windows.Forms.Padding(2);
            this.pnlResultUsers.Name = "pnlResultUsers";
            this.pnlResultUsers.Size = new System.Drawing.Size(80, 22);
            this.pnlResultUsers.TabIndex = 30;
            // 
            // lblRowCountUsers
            // 
            this.lblRowCountUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblRowCountUsers.AutoSize = true;
            this.lblRowCountUsers.ForeColor = System.Drawing.Color.White;
            this.lblRowCountUsers.Location = new System.Drawing.Point(2, 4);
            this.lblRowCountUsers.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRowCountUsers.Name = "lblRowCountUsers";
            this.lblRowCountUsers.Size = new System.Drawing.Size(49, 13);
            this.lblRowCountUsers.TabIndex = 9;
            this.lblRowCountUsers.Text = "Results: ";
            this.lblRowCountUsers.Click += new System.EventHandler(this.lblRowCountUsers_Click);
            // 
            // lblUserListLoad
            // 
            this.lblUserListLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblUserListLoad.AutoSize = true;
            this.lblUserListLoad.Font = new System.Drawing.Font("Bahnschrift SemiBold", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserListLoad.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblUserListLoad.Location = new System.Drawing.Point(79, 498);
            this.lblUserListLoad.Name = "lblUserListLoad";
            this.lblUserListLoad.Size = new System.Drawing.Size(109, 13);
            this.lblUserListLoad.TabIndex = 28;
            this.lblUserListLoad.Text = "initializing storelist...";
            this.lblUserListLoad.Click += new System.EventHandler(this.lblUserListLoad_Click);
            // 
            // lblCsvUserFileInfo
            // 
            this.lblCsvUserFileInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblCsvUserFileInfo.AutoSize = true;
            this.lblCsvUserFileInfo.Location = new System.Drawing.Point(79, 542);
            this.lblCsvUserFileInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCsvUserFileInfo.Name = "lblCsvUserFileInfo";
            this.lblCsvUserFileInfo.Size = new System.Drawing.Size(57, 13);
            this.lblCsvUserFileInfo.TabIndex = 27;
            this.lblCsvUserFileInfo.Text = "01/01/2001";
            this.lblCsvUserFileInfo.Click += new System.EventHandler(this.lblCsvUserFileInfo_Click);
            // 
            // lblFileUpdateUser
            // 
            this.lblFileUpdateUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblFileUpdateUser.AutoSize = true;
            this.lblFileUpdateUser.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFileUpdateUser.Location = new System.Drawing.Point(79, 525);
            this.lblFileUpdateUser.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFileUpdateUser.Name = "lblFileUpdateUser";
            this.lblFileUpdateUser.Size = new System.Drawing.Size(87, 13);
            this.lblFileUpdateUser.TabIndex = 26;
            this.lblFileUpdateUser.Text = "Last updated on:";
            this.lblFileUpdateUser.Click += new System.EventHandler(this.lblFileUpdateUser_Click);
            // 
            // btnReloadUserData
            // 
            this.btnReloadUserData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnReloadUserData.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnReloadUserData.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReloadUserData.ForeColor = System.Drawing.Color.White;
            this.btnReloadUserData.Location = new System.Drawing.Point(1, 496);
            this.btnReloadUserData.Margin = new System.Windows.Forms.Padding(2);
            this.btnReloadUserData.Name = "btnReloadUserData";
            this.btnReloadUserData.Size = new System.Drawing.Size(74, 60);
            this.btnReloadUserData.TabIndex = 18;
            this.btnReloadUserData.Text = "Reload";
            this.btnReloadUserData.UseVisualStyleBackColor = false;
            this.btnReloadUserData.Click += new System.EventHandler(this.btnReloadUserData_Click);
            // 
            // pnlUserIDs
            // 
            this.pnlUserIDs.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlUserIDs.BackColor = System.Drawing.Color.SlateGray;
            this.pnlUserIDs.Controls.Add(this.lblTVUserPwD);
            this.pnlUserIDs.Controls.Add(this.chbDisplayUserPwD);
            this.pnlUserIDs.Controls.Add(this.txtUserPwD);
            this.pnlUserIDs.Controls.Add(this.lblUserID);
            this.pnlUserIDs.Controls.Add(this.txtSelectedUserPC);
            this.pnlUserIDs.Enabled = false;
            this.pnlUserIDs.Location = new System.Drawing.Point(82, 468);
            this.pnlUserIDs.Margin = new System.Windows.Forms.Padding(2);
            this.pnlUserIDs.Name = "pnlUserIDs";
            this.pnlUserIDs.Size = new System.Drawing.Size(925, 22);
            this.pnlUserIDs.TabIndex = 11;
            this.pnlUserIDs.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlResultUsers_Paint);
            // 
            // lblTVUserPwD
            // 
            this.lblTVUserPwD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTVUserPwD.AutoSize = true;
            this.lblTVUserPwD.ForeColor = System.Drawing.Color.White;
            this.lblTVUserPwD.Location = new System.Drawing.Point(767, 4);
            this.lblTVUserPwD.Name = "lblTVUserPwD";
            this.lblTVUserPwD.Size = new System.Drawing.Size(60, 13);
            this.lblTVUserPwD.TabIndex = 23;
            this.lblTVUserPwD.Text = "Password:";
            this.lblTVUserPwD.Click += new System.EventHandler(this.lblTVUserPwD_Click);
            // 
            // chbDisplayUserPwD
            // 
            this.chbDisplayUserPwD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chbDisplayUserPwD.AutoSize = true;
            this.chbDisplayUserPwD.Checked = true;
            this.chbDisplayUserPwD.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbDisplayUserPwD.Location = new System.Drawing.Point(907, 5);
            this.chbDisplayUserPwD.Name = "chbDisplayUserPwD";
            this.chbDisplayUserPwD.Size = new System.Drawing.Size(15, 14);
            this.chbDisplayUserPwD.TabIndex = 22;
            this.chbDisplayUserPwD.UseVisualStyleBackColor = true;
            this.chbDisplayUserPwD.CheckedChanged += new System.EventHandler(this.chbDisplayUserPwD_CheckedChanged);
            // 
            // txtUserPwD
            // 
            this.txtUserPwD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUserPwD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtUserPwD.Location = new System.Drawing.Point(830, 4);
            this.txtUserPwD.Margin = new System.Windows.Forms.Padding(2);
            this.txtUserPwD.Name = "txtUserPwD";
            this.txtUserPwD.PasswordChar = '*';
            this.txtUserPwD.Size = new System.Drawing.Size(70, 14);
            this.txtUserPwD.TabIndex = 21;
            this.txtUserPwD.TextChanged += new System.EventHandler(this.txtUserPwD_TextChanged);
            // 
            // lblUserID
            // 
            this.lblUserID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserID.AutoSize = true;
            this.lblUserID.ForeColor = System.Drawing.Color.White;
            this.lblUserID.Location = new System.Drawing.Point(671, 4);
            this.lblUserID.Name = "lblUserID";
            this.lblUserID.Size = new System.Drawing.Size(19, 13);
            this.lblUserID.TabIndex = 20;
            this.lblUserID.Text = "ID:";
            this.lblUserID.Click += new System.EventHandler(this.lblUserID_Click);
            // 
            // txtSelectedUserPC
            // 
            this.txtSelectedUserPC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSelectedUserPC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSelectedUserPC.Location = new System.Drawing.Point(693, 4);
            this.txtSelectedUserPC.Margin = new System.Windows.Forms.Padding(2);
            this.txtSelectedUserPC.Name = "txtSelectedUserPC";
            this.txtSelectedUserPC.Size = new System.Drawing.Size(70, 14);
            this.txtSelectedUserPC.TabIndex = 8;
            this.txtSelectedUserPC.TextChanged += new System.EventHandler(this.txtSelectedUserPC_TextChanged);
            // 
            // dgvUsers
            // 
            this.dgvUsers.AllowUserToAddRows = false;
            this.dgvUsers.AllowUserToDeleteRows = false;
            this.dgvUsers.AllowUserToOrderColumns = true;
            this.dgvUsers.AllowUserToResizeRows = false;
            this.dgvUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvUsers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvUsers.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dgvUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsers.ContextMenuStrip = this.cntMenuStripCopyCell;
            this.dgvUsers.Location = new System.Drawing.Point(2, 2);
            this.dgvUsers.Margin = new System.Windows.Forms.Padding(2);
            this.dgvUsers.MultiSelect = false;
            this.dgvUsers.Name = "dgvUsers";
            this.dgvUsers.ReadOnly = true;
            this.dgvUsers.RowHeadersVisible = false;
            this.dgvUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUsers.Size = new System.Drawing.Size(1005, 466);
            this.dgvUsers.TabIndex = 1;
            this.dgvUsers.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUsers_CellContentClick);
            this.dgvUsers.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvUsers_RowsAdded);
            this.dgvUsers.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgvUsers_RowsRemoved);
            this.dgvUsers.SelectionChanged += new System.EventHandler(this.dgvUsers_SelectionChanged);
            // 
            // pnlUserButtons
            // 
            this.pnlUserButtons.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlUserButtons.Controls.Add(this.btnTeamViewerUser);
            this.pnlUserButtons.Controls.Add(this.btnPingPC);
            this.pnlUserButtons.Controls.Add(this.btnMapDrive);
            this.pnlUserButtons.Controls.Add(this.btnSkypeIM);
            this.pnlUserButtons.Controls.Add(this.btnSkypeUser);
            this.pnlUserButtons.Controls.Add(this.btnCellCall);
            this.pnlUserButtons.Enabled = false;
            this.pnlUserButtons.Location = new System.Drawing.Point(256, 493);
            this.pnlUserButtons.Name = "pnlUserButtons";
            this.pnlUserButtons.Size = new System.Drawing.Size(750, 65);
            this.pnlUserButtons.TabIndex = 29;
            this.pnlUserButtons.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlUserButtons_Paint);
            // 
            // btnTeamViewerUser
            // 
            this.btnTeamViewerUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTeamViewerUser.Location = new System.Drawing.Point(644, 11);
            this.btnTeamViewerUser.Margin = new System.Windows.Forms.Padding(2);
            this.btnTeamViewerUser.Name = "btnTeamViewerUser";
            this.btnTeamViewerUser.Size = new System.Drawing.Size(98, 41);
            this.btnTeamViewerUser.TabIndex = 25;
            this.btnTeamViewerUser.Text = "TeamViewer";
            this.btnTeamViewerUser.UseVisualStyleBackColor = true;
            this.btnTeamViewerUser.Click += new System.EventHandler(this.btnTeamViewerUser_Click);
            // 
            // btnPingPC
            // 
            this.btnPingPC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPingPC.Location = new System.Drawing.Point(539, 11);
            this.btnPingPC.Margin = new System.Windows.Forms.Padding(2);
            this.btnPingPC.Name = "btnPingPC";
            this.btnPingPC.Size = new System.Drawing.Size(98, 41);
            this.btnPingPC.TabIndex = 20;
            this.btnPingPC.Text = "Ping PC";
            this.btnPingPC.UseVisualStyleBackColor = true;
            this.btnPingPC.Click += new System.EventHandler(this.btnPingPC_Click_1);
            // 
            // btnMapDrive
            // 
            this.btnMapDrive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMapDrive.Location = new System.Drawing.Point(434, 11);
            this.btnMapDrive.Margin = new System.Windows.Forms.Padding(2);
            this.btnMapDrive.Name = "btnMapDrive";
            this.btnMapDrive.Size = new System.Drawing.Size(98, 41);
            this.btnMapDrive.TabIndex = 21;
            this.btnMapDrive.Text = "Map C Drive";
            this.btnMapDrive.UseVisualStyleBackColor = true;
            this.btnMapDrive.Click += new System.EventHandler(this.btnMapDrive_Click);
            // 
            // btnSkypeIM
            // 
            this.btnSkypeIM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSkypeIM.Location = new System.Drawing.Point(119, 11);
            this.btnSkypeIM.Margin = new System.Windows.Forms.Padding(2);
            this.btnSkypeIM.Name = "btnSkypeIM";
            this.btnSkypeIM.Size = new System.Drawing.Size(98, 41);
            this.btnSkypeIM.TabIndex = 24;
            this.btnSkypeIM.Text = "Skype IM";
            this.btnSkypeIM.UseVisualStyleBackColor = true;
            this.btnSkypeIM.Click += new System.EventHandler(this.btnSkypeIM_Click);
            // 
            // btnSkypeUser
            // 
            this.btnSkypeUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSkypeUser.Location = new System.Drawing.Point(329, 11);
            this.btnSkypeUser.Margin = new System.Windows.Forms.Padding(2);
            this.btnSkypeUser.Name = "btnSkypeUser";
            this.btnSkypeUser.Size = new System.Drawing.Size(98, 41);
            this.btnSkypeUser.TabIndex = 22;
            this.btnSkypeUser.Text = "Skype Call";
            this.btnSkypeUser.UseVisualStyleBackColor = true;
            this.btnSkypeUser.Click += new System.EventHandler(this.btnSkypeUser_Click);
            // 
            // btnCellCall
            // 
            this.btnCellCall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCellCall.Location = new System.Drawing.Point(224, 11);
            this.btnCellCall.Margin = new System.Windows.Forms.Padding(2);
            this.btnCellCall.Name = "btnCellCall";
            this.btnCellCall.Size = new System.Drawing.Size(98, 41);
            this.btnCellCall.TabIndex = 23;
            this.btnCellCall.Text = "Cell Call";
            this.btnCellCall.UseVisualStyleBackColor = true;
            this.btnCellCall.Click += new System.EventHandler(this.btnCellCall_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1015, 24);
            this.menuStrip1.TabIndex = 19;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadCSVToolStripMenuItem,
            this.configToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // loadCSVToolStripMenuItem
            // 
            this.loadCSVToolStripMenuItem.Image = global::SupportLauncher2.Properties.Resources.warning;
            this.loadCSVToolStripMenuItem.Name = "loadCSVToolStripMenuItem";
            this.loadCSVToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.loadCSVToolStripMenuItem.Text = "Load";
            this.loadCSVToolStripMenuItem.Click += new System.EventHandler(this.loadCSVToolStripMenuItem_Click);
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.configToolStripMenuItem.Text = "Config";
            this.configToolStripMenuItem.Click += new System.EventHandler(this.configToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // btnClearSearch
            // 
            this.btnClearSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearSearch.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnClearSearch.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnClearSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClearSearch.ForeColor = System.Drawing.SystemColors.Window;
            this.btnClearSearch.Location = new System.Drawing.Point(956, 31);
            this.btnClearSearch.Margin = new System.Windows.Forms.Padding(0);
            this.btnClearSearch.Name = "btnClearSearch";
            this.btnClearSearch.Size = new System.Drawing.Size(54, 21);
            this.btnClearSearch.TabIndex = 20;
            this.btnClearSearch.Text = "Clear";
            this.btnClearSearch.UseVisualStyleBackColor = false;
            this.btnClearSearch.Click += new System.EventHandler(this.btnClearSearch_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 645);
            this.Controls.Add(this.btnClearSearch);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblSearch);
            this.Controls.Add(this.txtSearch);
            this.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(950, 350);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SupportLauncher by BESTSELLER";
            ((System.ComponentModel.ISupportInitialize)(this.dgvStores)).EndInit();
            this.cntMenuStripCopyCell.ResumeLayout(false);
            this.pnlStoreIDs.ResumeLayout(false);
            this.pnlStoreIDs.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabStores.ResumeLayout(false);
            this.tabStores.PerformLayout();
            this.pnlResultStores.ResumeLayout(false);
            this.pnlResultStores.PerformLayout();
            this.pnlStoreButtons.ResumeLayout(false);
            this.tabUsers.ResumeLayout(false);
            this.tabUsers.PerformLayout();
            this.pnlResultUsers.ResumeLayout(false);
            this.pnlResultUsers.PerformLayout();
            this.pnlUserIDs.ResumeLayout(false);
            this.pnlUserIDs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsers)).EndInit();
            this.pnlUserButtons.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private TextBox txtSearch;
        private Label lblSearch;
        private Button btnTeamViewerStore;
        private DataGridView dgvStores;
        private TextBox txtSelectedStoreIP;
        private Label lblRowCountStores;
        private Panel pnlStoreIDs;
        private ContextMenuStrip cntMenuStripCopyCell;
        private ToolStripMenuItem copyToolStripMenuItem;
        private Button btnVNC;
        private Button btnPingStore;
        private Button btnPingNet;
        private Button btnCMD;
        private Button btnSQL;
        private Button btnReloadStoreData;
        private TabControl tabControl1;
        private TabPage tabStores;
        private TabPage tabUsers;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem configToolStripMenuItem;
        private ToolStripMenuItem exitToolStripMenuItem;
        private Panel pnlUserIDs;
        private TextBox txtSelectedUserPC;
        private Label lblRowCountUsers;
        private DataGridView dgvUsers;
        private Button btnClearSearch;
        private Button btnReloadUserData;
        private Button btnSkypeIM;
        private Button btnCellCall;
        private Button btnSkypeUser;
        private Button btnMapDrive;
        private Button btnPingPC;
        private ToolStripMenuItem loadCSVToolStripMenuItem;
        private Label lblFileUpdateStore;
        private Label lblCsvStoreFileInfo;
        private Panel pnlStoreButtons;
        private RadioButton rdbStoreIP1;
        private RadioButton rdbStoreIP0;
        private RadioButton rdbStoreIP2;
        private RadioButton rdbStoreIP6;
        private RadioButton rdbStoreIP5;
        private RadioButton rdbStoreIP4;
        private RadioButton rdbStoreIP3;
        private Button btnSkypeCallStore;
        private Panel pnlResultStores;
        private Button btnTeamViewerUser;
        private Label lblFileUpdateUser;
        private Label lblCsvUserFileInfo;
        private Label lblStoreListLoad;
        private TextBox txtStorePwD;
        private CheckBox chbDisplayStorePwD;
        private Label lblTVStorePwD;
        private Label lblStoreID;
        private Label lblUserID;
        private CheckBox chbDisplayUserPwD;
        private TextBox txtUserPwD;
        private Label lblTVUserPwD;
        private Label lblUserListLoad;
        private Panel pnlUserButtons;
        private Panel pnlResultUsers;
    }
}

