﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Drawing2D;


namespace SupportLauncher2
{
    public partial class ConfigForm : Form
    {
        private Config config;
        private Form1 mainForm;

        public ConfigForm(Config config, Form1 mainForm)
        {
            this.config = config;
            this.mainForm = mainForm;
            
            InitializeComponent();
            load_Settings();


            this.FormClosing += ConfigForm_FormClosing;  
                        
        }

        private void load_Settings()
        {
            this.textBox1.Text = config.TestPath;
            this.textBox2.Text = config.TestPath2;
            this.textBox3.Text = config.TestPath3;
            this.textBox4.Text = config.TestPath4;

            this.txtStorePath.Text = config.CsvStoreFilePath;
            this.txtStorePW.Text = config.StoreDefaultPW;
            this.txtDbPW.Text = config.SqlPw;
            this.numStoreID.Value = config.StoreIPIndex;
            this.numStoreTel.Value = config.StorePhoneIndex;

            this.txtUserPath.Text = config.CsvUserFilePath;
            this.txtUserPW.Text = config.UserDefaultPW;
            this.numUserID.Value = config.UserBSIndex;
            this.numUserTel.Value = config.UserPhoneIndex;
            this.numUserMob.Value = config.UserMobileIndex;
            this.numUserSip.Value = config.UserSipIndex;

            this.txtTvPath.Text = config.TeamViewerPath;
            this.txtVncPath.Text = config.VncPath;
            this.txtSqlPath.Text = config.SqlPath;
            this.txtSkypePath.Text = config.SkypePath;
        }

        private void save_Settings()
        {
            config.CsvStoreFilePath = this.txtStorePath.Text;
            config.StoreDefaultPW = this.txtStorePW.Text;
            config.SqlPw = this.txtDbPW.Text;
            config.StoreIPIndex = (int) this.numStoreID.Value;
            config.StorePhoneIndex = (int)this.numStoreTel.Value;

            config.CsvUserFilePath = this.txtUserPath.Text;
            config.UserDefaultPW = this.txtUserPW.Text;
            config.UserBSIndex = (int)this.numUserID.Value;
            config.UserPhoneIndex = (int)this.numUserTel.Value;
            config.UserMobileIndex = (int)this.numUserMob.Value;
            config.UserSipIndex = (int)this.numUserSip.Value;

            config.TeamViewerPath = this.txtTvPath.Text;
            config.VncPath = this.txtVncPath.Text;
            config.SqlPath = this.txtSqlPath.Text;
            config.SkypePath = this.txtSkypePath.Text;

            config.save_Settings();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void radioButtons_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbCsv.Checked)
            {
                //pnlCsv.Enabled = true;
                //pnlCsv.Visible = true;
                //pnlDB.Enabled = false;
                //pnlDB.Visible = false;
            }
            if (rdbDB.Checked)
            {
                //pnlCsv.Enabled = false;
                //pnlCsv.Visible = false;
                //pnlDB.Enabled = true;
                //pnlDB.Visible = true;
            }
        }

        private void btnCnc_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ConfigForm_FormClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.Owner.Enabled = true;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            save_Settings();
            mainForm.ReloadForm();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            save_Settings();
            this.Close();
            Application.Restart();
        }

        private void chkStorePW_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                txtStorePW.PasswordChar = '*';
            else
                txtStorePW.PasswordChar = '\0';
        }

        private void chkDbPW_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                txtDbPW.PasswordChar = '*';
            else
                txtDbPW.PasswordChar = '\0';
        }

        private void chkUserPW_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
                txtUserPW.PasswordChar = '*';
            else
                txtUserPW.PasswordChar = '\0';
        }

        private void btnStoreBrowse_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.Filter = "CSV Files (*.csv)|*.csv|All files (*.*)|*.*";
            this.openFileDialog1.FileName = "BestsellerStores.csv";

            string filePath;

            DialogResult result = this.openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                filePath = this.openFileDialog1.FileName;
                string fileExtension = Path.GetExtension(filePath);
                if (fileExtension.Equals(".csv"))
                {
                    txtStorePath.Text = filePath;
                }                    
            }
        }

        private void btnUserBrowse_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.Filter = "CSV Files (*.csv)|*.csv|All files (*.*)|*.*";
            this.openFileDialog1.FileName = "BestsellerUsers.csv";

            string filePath;

            DialogResult result = this.openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                filePath = this.openFileDialog1.FileName;
                string fileExtension = Path.GetExtension(filePath);
                if (fileExtension.Equals(".csv"))
                {
                    txtUserPath.Text = filePath;
                }
            }
        }

        private void btnTvBrowse_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.Filter = "Exe Files (.exe)|*.exe|All Files (*.*)|*.*";
            this.openFileDialog1.FileName = "TeamViewer.exe";

            string filePath;

            DialogResult result = this.openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                filePath = this.openFileDialog1.FileName;
                string fileExtension = Path.GetExtension(filePath);
                if (fileExtension.Equals(".exe"))
                {
                    txtTvPath.Text = filePath;
                }
            }
        }

        private void btnVncBrowse_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.Filter = "Exe Files (.exe)|*.exe|All Files (*.*)|*.*";
            this.openFileDialog1.FileName = "vncviewer.exe";

            string filePath;

            DialogResult result = this.openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                filePath = this.openFileDialog1.FileName;
                string fileExtension = Path.GetExtension(filePath);
                if (fileExtension.Equals(".exe"))
                {
                    txtVncPath.Text = filePath;
                }
            }
        }

        private void btnSqlBrowse_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.Filter = "Exe Files (.exe)|*.exe|All Files (*.*)|*.*";
            this.openFileDialog1.FileName = "Ssms.exe";

            string filePath;

            DialogResult result = this.openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                filePath = this.openFileDialog1.FileName;
                string fileExtension = Path.GetExtension(filePath);
                if (fileExtension.Equals(".exe"))
                {
                    txtSqlPath.Text = filePath;
                }
            }
        }

        private void btnSkypeBrowse_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.Filter = "Exe Files (.exe)|*.exe|All Files (*.*)|*.*";
            this.openFileDialog1.FileName = "lync.exe";

            string filePath;

            DialogResult result = this.openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                filePath = this.openFileDialog1.FileName;
                string fileExtension = Path.GetExtension(filePath);
                if (fileExtension.Equals(".exe"))
                {
                    txtSkypePath.Text = filePath;
                }
            }
        }
    }    
}
