﻿using System.Drawing;
using System.Windows.Forms;

namespace SupportLauncher2
{
    partial class ConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigForm));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbpData = new System.Windows.Forms.TabPage();
            this.pnlCsv = new System.Windows.Forms.Panel();
            this.grpBoxUsers = new System.Windows.Forms.GroupBox();
            this.numUserSip = new System.Windows.Forms.NumericUpDown();
            this.numUserMob = new System.Windows.Forms.NumericUpDown();
            this.numUserTel = new System.Windows.Forms.NumericUpDown();
            this.numUserID = new System.Windows.Forms.NumericUpDown();
            this.lblUserSip = new System.Windows.Forms.Label();
            this.lblUserMob = new System.Windows.Forms.Label();
            this.lblUserTel = new System.Windows.Forms.Label();
            this.grpBoxUsersInfo = new System.Windows.Forms.GroupBox();
            this.chkUserPW = new System.Windows.Forms.CheckBox();
            this.btnUserBrowse = new System.Windows.Forms.Button();
            this.txtUserPW = new System.Windows.Forms.TextBox();
            this.txtUserPath = new System.Windows.Forms.TextBox();
            this.lblUserPW = new System.Windows.Forms.Label();
            this.lblUserPath = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.grpBoxStores = new System.Windows.Forms.GroupBox();
            this.numStoreTel = new System.Windows.Forms.NumericUpDown();
            this.numStoreID = new System.Windows.Forms.NumericUpDown();
            this.lblStoreTel = new System.Windows.Forms.Label();
            this.lblStoreId = new System.Windows.Forms.Label();
            this.grpBoxStoreInfo = new System.Windows.Forms.GroupBox();
            this.chkDbPW = new System.Windows.Forms.CheckBox();
            this.txtDbPW = new System.Windows.Forms.TextBox();
            this.lblDbPW = new System.Windows.Forms.Label();
            this.chkStorePW = new System.Windows.Forms.CheckBox();
            this.txtStorePW = new System.Windows.Forms.TextBox();
            this.btnStoreBrowse = new System.Windows.Forms.Button();
            this.txtStorePath = new System.Windows.Forms.TextBox();
            this.lblStorePW = new System.Windows.Forms.Label();
            this.lblStorePath = new System.Windows.Forms.Label();
            this.grpBoxSource = new System.Windows.Forms.GroupBox();
            this.rdbDB = new System.Windows.Forms.RadioButton();
            this.rdbCsv = new System.Windows.Forms.RadioButton();
            this.tbpPrograms = new System.Windows.Forms.TabPage();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCnc = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.grpBoxSkype = new Cyotek.Framework.Application.Components.GroupBox();
            this.grpBoxSkypePath = new System.Windows.Forms.GroupBox();
            this.txtSkypePath = new System.Windows.Forms.TextBox();
            this.btnSkypeBrowse = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.grpBoxSQL = new Cyotek.Framework.Application.Components.GroupBox();
            this.grpBoxSqlPath = new System.Windows.Forms.GroupBox();
            this.txtSqlPath = new System.Windows.Forms.TextBox();
            this.btnSqlBrowse = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.grpBoxVNC = new Cyotek.Framework.Application.Components.GroupBox();
            this.grpBoxVncPath = new System.Windows.Forms.GroupBox();
            this.txtVncPath = new System.Windows.Forms.TextBox();
            this.btnVncBrowse = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.grpBoxTV = new Cyotek.Framework.Application.Components.GroupBox();
            this.grpBoxTvPath = new System.Windows.Forms.GroupBox();
            this.txtTvPath = new System.Windows.Forms.TextBox();
            this.btnTvBrowse = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tbpData.SuspendLayout();
            this.pnlCsv.SuspendLayout();
            this.grpBoxUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUserSip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUserMob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUserTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUserID)).BeginInit();
            this.grpBoxUsersInfo.SuspendLayout();
            this.grpBoxStores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStoreTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStoreID)).BeginInit();
            this.grpBoxStoreInfo.SuspendLayout();
            this.grpBoxSource.SuspendLayout();
            this.tbpPrograms.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.grpBoxSkype.SuspendLayout();
            this.grpBoxSkypePath.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.grpBoxSQL.SuspendLayout();
            this.grpBoxSqlPath.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.grpBoxVNC.SuspendLayout();
            this.grpBoxVncPath.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.grpBoxTV.SuspendLayout();
            this.grpBoxTvPath.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbpData);
            this.tabControl1.Controls.Add(this.tbpPrograms);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(2, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(455, 464);
            this.tabControl1.TabIndex = 0;
            // 
            // tbpData
            // 
            this.tbpData.BackColor = System.Drawing.Color.Transparent;
            this.tbpData.Controls.Add(this.pnlCsv);
            this.tbpData.Controls.Add(this.grpBoxSource);
            this.tbpData.Location = new System.Drawing.Point(4, 22);
            this.tbpData.Name = "tbpData";
            this.tbpData.Padding = new System.Windows.Forms.Padding(3);
            this.tbpData.Size = new System.Drawing.Size(447, 438);
            this.tbpData.TabIndex = 0;
            this.tbpData.Text = "Datasource";
            // 
            // pnlCsv
            // 
            this.pnlCsv.BackColor = System.Drawing.Color.Transparent;
            this.pnlCsv.Controls.Add(this.grpBoxUsers);
            this.pnlCsv.Controls.Add(this.grpBoxStores);
            this.pnlCsv.Location = new System.Drawing.Point(0, 64);
            this.pnlCsv.Name = "pnlCsv";
            this.pnlCsv.Size = new System.Drawing.Size(447, 376);
            this.pnlCsv.TabIndex = 1;
            // 
            // grpBoxUsers
            // 
            this.grpBoxUsers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(248)))), ((int)(((byte)(252)))));
            this.grpBoxUsers.Controls.Add(this.numUserSip);
            this.grpBoxUsers.Controls.Add(this.numUserMob);
            this.grpBoxUsers.Controls.Add(this.numUserTel);
            this.grpBoxUsers.Controls.Add(this.numUserID);
            this.grpBoxUsers.Controls.Add(this.lblUserSip);
            this.grpBoxUsers.Controls.Add(this.lblUserMob);
            this.grpBoxUsers.Controls.Add(this.lblUserTel);
            this.grpBoxUsers.Controls.Add(this.grpBoxUsersInfo);
            this.grpBoxUsers.Controls.Add(this.lblUserId);
            this.grpBoxUsers.Location = new System.Drawing.Point(4, 188);
            this.grpBoxUsers.Name = "grpBoxUsers";
            this.grpBoxUsers.Size = new System.Drawing.Size(437, 182);
            this.grpBoxUsers.TabIndex = 1;
            this.grpBoxUsers.TabStop = false;
            this.grpBoxUsers.Text = "[ConnectionListUsers]";
            // 
            // numUserSip
            // 
            this.numUserSip.BackColor = System.Drawing.Color.White;
            this.numUserSip.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numUserSip.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUserSip.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.numUserSip.Location = new System.Drawing.Point(194, 156);
            this.numUserSip.Name = "numUserSip";
            this.numUserSip.Size = new System.Drawing.Size(211, 18);
            this.numUserSip.TabIndex = 9;
            // 
            // numUserMob
            // 
            this.numUserMob.BackColor = System.Drawing.Color.White;
            this.numUserMob.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numUserMob.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUserMob.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.numUserMob.Location = new System.Drawing.Point(194, 134);
            this.numUserMob.Name = "numUserMob";
            this.numUserMob.Size = new System.Drawing.Size(211, 18);
            this.numUserMob.TabIndex = 8;
            // 
            // numUserTel
            // 
            this.numUserTel.BackColor = System.Drawing.Color.White;
            this.numUserTel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numUserTel.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUserTel.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.numUserTel.Location = new System.Drawing.Point(194, 112);
            this.numUserTel.Name = "numUserTel";
            this.numUserTel.Size = new System.Drawing.Size(211, 18);
            this.numUserTel.TabIndex = 7;
            // 
            // numUserID
            // 
            this.numUserID.BackColor = System.Drawing.Color.White;
            this.numUserID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numUserID.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUserID.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.numUserID.Location = new System.Drawing.Point(194, 90);
            this.numUserID.Name = "numUserID";
            this.numUserID.Size = new System.Drawing.Size(211, 18);
            this.numUserID.TabIndex = 5;
            // 
            // lblUserSip
            // 
            this.lblUserSip.AutoSize = true;
            this.lblUserSip.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserSip.Location = new System.Drawing.Point(13, 159);
            this.lblUserSip.Name = "lblUserSip";
            this.lblUserSip.Size = new System.Drawing.Size(61, 13);
            this.lblUserSip.TabIndex = 6;
            this.lblUserSip.Text = "sipColoumn";
            // 
            // lblUserMob
            // 
            this.lblUserMob.AutoSize = true;
            this.lblUserMob.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserMob.Location = new System.Drawing.Point(13, 137);
            this.lblUserMob.Name = "lblUserMob";
            this.lblUserMob.Size = new System.Drawing.Size(78, 13);
            this.lblUserMob.TabIndex = 5;
            this.lblUserMob.Text = "mobileColoumn";
            // 
            // lblUserTel
            // 
            this.lblUserTel.AutoSize = true;
            this.lblUserTel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserTel.Location = new System.Drawing.Point(13, 115);
            this.lblUserTel.Name = "lblUserTel";
            this.lblUserTel.Size = new System.Drawing.Size(59, 13);
            this.lblUserTel.TabIndex = 4;
            this.lblUserTel.Text = "telColoumn";
            // 
            // grpBoxUsersInfo
            // 
            this.grpBoxUsersInfo.Controls.Add(this.chkUserPW);
            this.grpBoxUsersInfo.Controls.Add(this.btnUserBrowse);
            this.grpBoxUsersInfo.Controls.Add(this.txtUserPW);
            this.grpBoxUsersInfo.Controls.Add(this.txtUserPath);
            this.grpBoxUsersInfo.Controls.Add(this.lblUserPW);
            this.grpBoxUsersInfo.Controls.Add(this.lblUserPath);
            this.grpBoxUsersInfo.Location = new System.Drawing.Point(7, 19);
            this.grpBoxUsersInfo.Name = "grpBoxUsersInfo";
            this.grpBoxUsersInfo.Size = new System.Drawing.Size(424, 65);
            this.grpBoxUsersInfo.TabIndex = 1;
            this.grpBoxUsersInfo.TabStop = false;
            // 
            // chkUserPW
            // 
            this.chkUserPW.AutoSize = true;
            this.chkUserPW.Checked = true;
            this.chkUserPW.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUserPW.Location = new System.Drawing.Point(385, 41);
            this.chkUserPW.Name = "chkUserPW";
            this.chkUserPW.Size = new System.Drawing.Size(15, 14);
            this.chkUserPW.TabIndex = 6;
            this.chkUserPW.UseVisualStyleBackColor = true;
            this.chkUserPW.CheckedChanged += new System.EventHandler(this.chkUserPW_CheckedChanged);
            // 
            // btnUserBrowse
            // 
            this.btnUserBrowse.Location = new System.Drawing.Point(385, 10);
            this.btnUserBrowse.Name = "btnUserBrowse";
            this.btnUserBrowse.Size = new System.Drawing.Size(30, 26);
            this.btnUserBrowse.TabIndex = 6;
            this.btnUserBrowse.Text = "...";
            this.btnUserBrowse.UseVisualStyleBackColor = true;
            this.btnUserBrowse.Click += new System.EventHandler(this.btnUserBrowse_Click);
            // 
            // txtUserPW
            // 
            this.txtUserPW.BackColor = System.Drawing.Color.White;
            this.txtUserPW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserPW.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserPW.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtUserPW.Location = new System.Drawing.Point(187, 37);
            this.txtUserPW.Name = "txtUserPW";
            this.txtUserPW.PasswordChar = '*';
            this.txtUserPW.Size = new System.Drawing.Size(192, 20);
            this.txtUserPW.TabIndex = 6;
            // 
            // txtUserPath
            // 
            this.txtUserPath.BackColor = System.Drawing.Color.White;
            this.txtUserPath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserPath.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtUserPath.Location = new System.Drawing.Point(187, 11);
            this.txtUserPath.Name = "txtUserPath";
            this.txtUserPath.Size = new System.Drawing.Size(192, 20);
            this.txtUserPath.TabIndex = 6;
            // 
            // lblUserPW
            // 
            this.lblUserPW.AutoSize = true;
            this.lblUserPW.Location = new System.Drawing.Point(6, 40);
            this.lblUserPW.Name = "lblUserPW";
            this.lblUserPW.Size = new System.Drawing.Size(79, 13);
            this.lblUserPW.TabIndex = 3;
            this.lblUserPW.Text = "defaultUserPW";
            // 
            // lblUserPath
            // 
            this.lblUserPath.AutoSize = true;
            this.lblUserPath.Location = new System.Drawing.Point(6, 16);
            this.lblUserPath.Name = "lblUserPath";
            this.lblUserPath.Size = new System.Drawing.Size(28, 13);
            this.lblUserPath.TabIndex = 2;
            this.lblUserPath.Text = "path";
            // 
            // lblUserId
            // 
            this.lblUserId.AutoSize = true;
            this.lblUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserId.Location = new System.Drawing.Point(13, 93);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(56, 13);
            this.lblUserId.TabIndex = 3;
            this.lblUserId.Text = "idColoumn";
            // 
            // grpBoxStores
            // 
            this.grpBoxStores.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(248)))), ((int)(((byte)(252)))));
            this.grpBoxStores.Controls.Add(this.numStoreTel);
            this.grpBoxStores.Controls.Add(this.numStoreID);
            this.grpBoxStores.Controls.Add(this.lblStoreTel);
            this.grpBoxStores.Controls.Add(this.lblStoreId);
            this.grpBoxStores.Controls.Add(this.grpBoxStoreInfo);
            this.grpBoxStores.Location = new System.Drawing.Point(4, 4);
            this.grpBoxStores.Name = "grpBoxStores";
            this.grpBoxStores.Size = new System.Drawing.Size(437, 168);
            this.grpBoxStores.TabIndex = 0;
            this.grpBoxStores.TabStop = false;
            this.grpBoxStores.Text = "[ConnectionListStores]";
            // 
            // numStoreTel
            // 
            this.numStoreTel.BackColor = System.Drawing.Color.White;
            this.numStoreTel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numStoreTel.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numStoreTel.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.numStoreTel.Location = new System.Drawing.Point(194, 143);
            this.numStoreTel.Name = "numStoreTel";
            this.numStoreTel.Size = new System.Drawing.Size(211, 18);
            this.numStoreTel.TabIndex = 4;
            // 
            // numStoreID
            // 
            this.numStoreID.BackColor = System.Drawing.Color.White;
            this.numStoreID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.numStoreID.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numStoreID.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.numStoreID.Location = new System.Drawing.Point(194, 120);
            this.numStoreID.Name = "numStoreID";
            this.numStoreID.Size = new System.Drawing.Size(211, 18);
            this.numStoreID.TabIndex = 3;
            // 
            // lblStoreTel
            // 
            this.lblStoreTel.AutoSize = true;
            this.lblStoreTel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStoreTel.Location = new System.Drawing.Point(13, 145);
            this.lblStoreTel.Name = "lblStoreTel";
            this.lblStoreTel.Size = new System.Drawing.Size(59, 13);
            this.lblStoreTel.TabIndex = 2;
            this.lblStoreTel.Text = "telColoumn";
            // 
            // lblStoreId
            // 
            this.lblStoreId.AutoSize = true;
            this.lblStoreId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStoreId.Location = new System.Drawing.Point(13, 122);
            this.lblStoreId.Name = "lblStoreId";
            this.lblStoreId.Size = new System.Drawing.Size(56, 13);
            this.lblStoreId.TabIndex = 1;
            this.lblStoreId.Text = "idColoumn";
            // 
            // grpBoxStoreInfo
            // 
            this.grpBoxStoreInfo.BackColor = System.Drawing.Color.Transparent;
            this.grpBoxStoreInfo.Controls.Add(this.chkDbPW);
            this.grpBoxStoreInfo.Controls.Add(this.txtDbPW);
            this.grpBoxStoreInfo.Controls.Add(this.lblDbPW);
            this.grpBoxStoreInfo.Controls.Add(this.chkStorePW);
            this.grpBoxStoreInfo.Controls.Add(this.txtStorePW);
            this.grpBoxStoreInfo.Controls.Add(this.btnStoreBrowse);
            this.grpBoxStoreInfo.Controls.Add(this.txtStorePath);
            this.grpBoxStoreInfo.Controls.Add(this.lblStorePW);
            this.grpBoxStoreInfo.Controls.Add(this.lblStorePath);
            this.grpBoxStoreInfo.Location = new System.Drawing.Point(6, 19);
            this.grpBoxStoreInfo.Name = "grpBoxStoreInfo";
            this.grpBoxStoreInfo.Size = new System.Drawing.Size(424, 95);
            this.grpBoxStoreInfo.TabIndex = 0;
            this.grpBoxStoreInfo.TabStop = false;
            // 
            // chkDbPW
            // 
            this.chkDbPW.AutoSize = true;
            this.chkDbPW.Checked = true;
            this.chkDbPW.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkDbPW.Location = new System.Drawing.Point(386, 68);
            this.chkDbPW.Name = "chkDbPW";
            this.chkDbPW.Size = new System.Drawing.Size(15, 14);
            this.chkDbPW.TabIndex = 8;
            this.chkDbPW.UseVisualStyleBackColor = true;
            this.chkDbPW.CheckedChanged += new System.EventHandler(this.chkDbPW_CheckedChanged);
            // 
            // txtDbPW
            // 
            this.txtDbPW.BackColor = System.Drawing.Color.White;
            this.txtDbPW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDbPW.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDbPW.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtDbPW.Location = new System.Drawing.Point(188, 63);
            this.txtDbPW.Name = "txtDbPW";
            this.txtDbPW.PasswordChar = '*';
            this.txtDbPW.Size = new System.Drawing.Size(192, 20);
            this.txtDbPW.TabIndex = 7;
            // 
            // lblDbPW
            // 
            this.lblDbPW.AutoSize = true;
            this.lblDbPW.Location = new System.Drawing.Point(7, 66);
            this.lblDbPW.Name = "lblDbPW";
            this.lblDbPW.Size = new System.Drawing.Size(97, 13);
            this.lblDbPW.TabIndex = 6;
            this.lblDbPW.Text = "databasePassword";
            // 
            // chkStorePW
            // 
            this.chkStorePW.AutoSize = true;
            this.chkStorePW.Checked = true;
            this.chkStorePW.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStorePW.Location = new System.Drawing.Point(386, 42);
            this.chkStorePW.Name = "chkStorePW";
            this.chkStorePW.Size = new System.Drawing.Size(15, 14);
            this.chkStorePW.TabIndex = 5;
            this.chkStorePW.UseVisualStyleBackColor = true;
            this.chkStorePW.CheckedChanged += new System.EventHandler(this.chkStorePW_CheckedChanged);
            // 
            // txtStorePW
            // 
            this.txtStorePW.BackColor = System.Drawing.Color.White;
            this.txtStorePW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStorePW.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStorePW.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtStorePW.Location = new System.Drawing.Point(188, 37);
            this.txtStorePW.Name = "txtStorePW";
            this.txtStorePW.PasswordChar = '*';
            this.txtStorePW.Size = new System.Drawing.Size(192, 20);
            this.txtStorePW.TabIndex = 4;
            // 
            // btnStoreBrowse
            // 
            this.btnStoreBrowse.Location = new System.Drawing.Point(386, 10);
            this.btnStoreBrowse.Name = "btnStoreBrowse";
            this.btnStoreBrowse.Size = new System.Drawing.Size(30, 26);
            this.btnStoreBrowse.TabIndex = 3;
            this.btnStoreBrowse.Text = "...";
            this.btnStoreBrowse.UseVisualStyleBackColor = true;
            this.btnStoreBrowse.Click += new System.EventHandler(this.btnStoreBrowse_Click);
            // 
            // txtStorePath
            // 
            this.txtStorePath.BackColor = System.Drawing.Color.White;
            this.txtStorePath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStorePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStorePath.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtStorePath.Location = new System.Drawing.Point(188, 11);
            this.txtStorePath.Name = "txtStorePath";
            this.txtStorePath.Size = new System.Drawing.Size(192, 20);
            this.txtStorePath.TabIndex = 2;
            // 
            // lblStorePW
            // 
            this.lblStorePW.AutoSize = true;
            this.lblStorePW.Location = new System.Drawing.Point(7, 40);
            this.lblStorePW.Name = "lblStorePW";
            this.lblStorePW.Size = new System.Drawing.Size(82, 13);
            this.lblStorePW.TabIndex = 1;
            this.lblStorePW.Text = "defaultStorePW";
            // 
            // lblStorePath
            // 
            this.lblStorePath.AutoSize = true;
            this.lblStorePath.Location = new System.Drawing.Point(7, 15);
            this.lblStorePath.Name = "lblStorePath";
            this.lblStorePath.Size = new System.Drawing.Size(28, 13);
            this.lblStorePath.TabIndex = 0;
            this.lblStorePath.Text = "path";
            // 
            // grpBoxSource
            // 
            this.grpBoxSource.BackColor = System.Drawing.Color.Transparent;
            this.grpBoxSource.Controls.Add(this.rdbDB);
            this.grpBoxSource.Controls.Add(this.rdbCsv);
            this.grpBoxSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxSource.Location = new System.Drawing.Point(4, 10);
            this.grpBoxSource.Name = "grpBoxSource";
            this.grpBoxSource.Size = new System.Drawing.Size(436, 50);
            this.grpBoxSource.TabIndex = 0;
            this.grpBoxSource.TabStop = false;
            this.grpBoxSource.Text = "Data source connection";
            // 
            // rdbDB
            // 
            this.rdbDB.AutoSize = true;
            this.rdbDB.Enabled = false;
            this.rdbDB.Location = new System.Drawing.Point(92, 22);
            this.rdbDB.Name = "rdbDB";
            this.rdbDB.Size = new System.Drawing.Size(71, 17);
            this.rdbDB.TabIndex = 1;
            this.rdbDB.Text = "Database";
            this.rdbDB.UseVisualStyleBackColor = true;
            this.rdbDB.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // rdbCsv
            // 
            this.rdbCsv.AutoSize = true;
            this.rdbCsv.Checked = true;
            this.rdbCsv.Location = new System.Drawing.Point(21, 22);
            this.rdbCsv.Name = "rdbCsv";
            this.rdbCsv.Size = new System.Drawing.Size(59, 17);
            this.rdbCsv.TabIndex = 0;
            this.rdbCsv.TabStop = true;
            this.rdbCsv.Text = "Csv file";
            this.rdbCsv.UseVisualStyleBackColor = true;
            this.rdbCsv.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // tbpPrograms
            // 
            this.tbpPrograms.BackColor = System.Drawing.Color.Transparent;
            this.tbpPrograms.Controls.Add(this.grpBoxSkype);
            this.tbpPrograms.Controls.Add(this.grpBoxSQL);
            this.tbpPrograms.Controls.Add(this.grpBoxVNC);
            this.tbpPrograms.Controls.Add(this.grpBoxTV);
            this.tbpPrograms.Location = new System.Drawing.Point(4, 22);
            this.tbpPrograms.Name = "tbpPrograms";
            this.tbpPrograms.Padding = new System.Windows.Forms.Padding(3);
            this.tbpPrograms.Size = new System.Drawing.Size(447, 438);
            this.tbpPrograms.TabIndex = 1;
            this.tbpPrograms.Text = "Programs";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox4);
            this.tabPage1.Controls.Add(this.textBox3);
            this.tabPage1.Controls.Add(this.textBox2);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(447, 438);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(7, 219);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(433, 20);
            this.textBox4.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(6, 158);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(434, 20);
            this.textBox3.TabIndex = 2;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(6, 104);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(435, 20);
            this.textBox2.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(3, 45);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(437, 20);
            this.textBox1.TabIndex = 0;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(216, 482);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCnc
            // 
            this.btnCnc.Location = new System.Drawing.Point(297, 482);
            this.btnCnc.Name = "btnCnc";
            this.btnCnc.Size = new System.Drawing.Size(75, 23);
            this.btnCnc.TabIndex = 2;
            this.btnCnc.Text = "Cancel";
            this.btnCnc.UseVisualStyleBackColor = true;
            this.btnCnc.Click += new System.EventHandler(this.btnCnc_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(378, 482);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 3;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // grpBoxSkype
            // 
            this.grpBoxSkype.Controls.Add(this.grpBoxSkypePath);
            this.grpBoxSkype.Controls.Add(this.pictureBox3);
            this.grpBoxSkype.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxSkype.Location = new System.Drawing.Point(7, 335);
            this.grpBoxSkype.Name = "grpBoxSkype";
            this.grpBoxSkype.Size = new System.Drawing.Size(433, 95);
            this.grpBoxSkype.TabIndex = 4;
            this.grpBoxSkype.TabStop = false;
            this.grpBoxSkype.Text = "Microsoft Skype";
            // 
            // grpBoxSkypePath
            // 
            this.grpBoxSkypePath.BackColor = System.Drawing.Color.Transparent;
            this.grpBoxSkypePath.Controls.Add(this.txtSkypePath);
            this.grpBoxSkypePath.Controls.Add(this.btnSkypeBrowse);
            this.grpBoxSkypePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxSkypePath.Location = new System.Drawing.Point(90, 20);
            this.grpBoxSkypePath.Name = "grpBoxSkypePath";
            this.grpBoxSkypePath.Size = new System.Drawing.Size(340, 69);
            this.grpBoxSkypePath.TabIndex = 3;
            this.grpBoxSkypePath.TabStop = false;
            this.grpBoxSkypePath.Text = "path";
            // 
            // txtSkypePath
            // 
            this.txtSkypePath.BackColor = System.Drawing.SystemColors.Control;
            this.txtSkypePath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSkypePath.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtSkypePath.Location = new System.Drawing.Point(12, 38);
            this.txtSkypePath.Name = "txtSkypePath";
            this.txtSkypePath.ReadOnly = true;
            this.txtSkypePath.Size = new System.Drawing.Size(241, 20);
            this.txtSkypePath.TabIndex = 1;
            // 
            // btnSkypeBrowse
            // 
            this.btnSkypeBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSkypeBrowse.Location = new System.Drawing.Point(260, 36);
            this.btnSkypeBrowse.Name = "btnSkypeBrowse";
            this.btnSkypeBrowse.Size = new System.Drawing.Size(74, 24);
            this.btnSkypeBrowse.TabIndex = 2;
            this.btnSkypeBrowse.Text = "Browse";
            this.btnSkypeBrowse.UseVisualStyleBackColor = true;
            this.btnSkypeBrowse.Click += new System.EventHandler(this.btnSkypeBrowse_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::SupportLauncher2.Properties.Resources.skype_bw_48x48_logo;
            this.pictureBox3.Location = new System.Drawing.Point(25, 31);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(48, 48);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // grpBoxSQL
            // 
            this.grpBoxSQL.Controls.Add(this.grpBoxSqlPath);
            this.grpBoxSQL.Controls.Add(this.pictureBox4);
            this.grpBoxSQL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxSQL.Location = new System.Drawing.Point(7, 229);
            this.grpBoxSQL.Name = "grpBoxSQL";
            this.grpBoxSQL.Size = new System.Drawing.Size(433, 95);
            this.grpBoxSQL.TabIndex = 4;
            this.grpBoxSQL.TabStop = false;
            this.grpBoxSQL.Text = "Microsoft SQL Manager";
            // 
            // grpBoxSqlPath
            // 
            this.grpBoxSqlPath.BackColor = System.Drawing.Color.Transparent;
            this.grpBoxSqlPath.Controls.Add(this.txtSqlPath);
            this.grpBoxSqlPath.Controls.Add(this.btnSqlBrowse);
            this.grpBoxSqlPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxSqlPath.Location = new System.Drawing.Point(90, 19);
            this.grpBoxSqlPath.Name = "grpBoxSqlPath";
            this.grpBoxSqlPath.Size = new System.Drawing.Size(340, 69);
            this.grpBoxSqlPath.TabIndex = 3;
            this.grpBoxSqlPath.TabStop = false;
            this.grpBoxSqlPath.Text = "path";
            // 
            // txtSqlPath
            // 
            this.txtSqlPath.BackColor = System.Drawing.SystemColors.Control;
            this.txtSqlPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSqlPath.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtSqlPath.Location = new System.Drawing.Point(12, 38);
            this.txtSqlPath.Name = "txtSqlPath";
            this.txtSqlPath.ReadOnly = true;
            this.txtSqlPath.Size = new System.Drawing.Size(241, 20);
            this.txtSqlPath.TabIndex = 1;
            // 
            // btnSqlBrowse
            // 
            this.btnSqlBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSqlBrowse.Location = new System.Drawing.Point(260, 36);
            this.btnSqlBrowse.Name = "btnSqlBrowse";
            this.btnSqlBrowse.Size = new System.Drawing.Size(74, 24);
            this.btnSqlBrowse.TabIndex = 2;
            this.btnSqlBrowse.Text = "Browse";
            this.btnSqlBrowse.UseVisualStyleBackColor = true;
            this.btnSqlBrowse.Click += new System.EventHandler(this.btnSqlBrowse_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::SupportLauncher2.Properties.Resources.sql_bw_48x48_logo_2;
            this.pictureBox4.Location = new System.Drawing.Point(25, 32);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(48, 48);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox4.TabIndex = 0;
            this.pictureBox4.TabStop = false;
            // 
            // grpBoxVNC
            // 
            this.grpBoxVNC.Controls.Add(this.grpBoxVncPath);
            this.grpBoxVNC.Controls.Add(this.pictureBox2);
            this.grpBoxVNC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxVNC.Location = new System.Drawing.Point(7, 123);
            this.grpBoxVNC.Name = "grpBoxVNC";
            this.grpBoxVNC.Size = new System.Drawing.Size(433, 95);
            this.grpBoxVNC.TabIndex = 3;
            this.grpBoxVNC.TabStop = false;
            this.grpBoxVNC.Text = "VNC Client";
            // 
            // grpBoxVncPath
            // 
            this.grpBoxVncPath.BackColor = System.Drawing.Color.Transparent;
            this.grpBoxVncPath.Controls.Add(this.txtVncPath);
            this.grpBoxVncPath.Controls.Add(this.btnVncBrowse);
            this.grpBoxVncPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxVncPath.Location = new System.Drawing.Point(90, 19);
            this.grpBoxVncPath.Name = "grpBoxVncPath";
            this.grpBoxVncPath.Size = new System.Drawing.Size(340, 69);
            this.grpBoxVncPath.TabIndex = 3;
            this.grpBoxVncPath.TabStop = false;
            this.grpBoxVncPath.Text = "path";
            // 
            // txtVncPath
            // 
            this.txtVncPath.BackColor = System.Drawing.SystemColors.Control;
            this.txtVncPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVncPath.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtVncPath.Location = new System.Drawing.Point(12, 38);
            this.txtVncPath.Name = "txtVncPath";
            this.txtVncPath.ReadOnly = true;
            this.txtVncPath.Size = new System.Drawing.Size(241, 20);
            this.txtVncPath.TabIndex = 1;
            // 
            // btnVncBrowse
            // 
            this.btnVncBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVncBrowse.Location = new System.Drawing.Point(260, 36);
            this.btnVncBrowse.Name = "btnVncBrowse";
            this.btnVncBrowse.Size = new System.Drawing.Size(74, 24);
            this.btnVncBrowse.TabIndex = 2;
            this.btnVncBrowse.Text = "Browse";
            this.btnVncBrowse.UseVisualStyleBackColor = true;
            this.btnVncBrowse.Click += new System.EventHandler(this.btnVncBrowse_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::SupportLauncher2.Properties.Resources.UltraVNC_bw_48x48_logo;
            this.pictureBox2.Location = new System.Drawing.Point(25, 32);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(48, 48);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // grpBoxTV
            // 
            this.grpBoxTV.BackColor = System.Drawing.Color.Transparent;
            this.grpBoxTV.Controls.Add(this.grpBoxTvPath);
            this.grpBoxTV.Controls.Add(this.pictureBox1);
            this.grpBoxTV.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxTV.Location = new System.Drawing.Point(7, 17);
            this.grpBoxTV.Name = "grpBoxTV";
            this.grpBoxTV.Size = new System.Drawing.Size(436, 95);
            this.grpBoxTV.TabIndex = 2;
            this.grpBoxTV.TabStop = false;
            this.grpBoxTV.Text = "TeamViewer";
            // 
            // grpBoxTvPath
            // 
            this.grpBoxTvPath.BackColor = System.Drawing.Color.Transparent;
            this.grpBoxTvPath.Controls.Add(this.txtTvPath);
            this.grpBoxTvPath.Controls.Add(this.btnTvBrowse);
            this.grpBoxTvPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxTvPath.Location = new System.Drawing.Point(90, 19);
            this.grpBoxTvPath.Name = "grpBoxTvPath";
            this.grpBoxTvPath.Size = new System.Drawing.Size(340, 69);
            this.grpBoxTvPath.TabIndex = 1;
            this.grpBoxTvPath.TabStop = false;
            this.grpBoxTvPath.Text = "path";
            // 
            // txtTvPath
            // 
            this.txtTvPath.BackColor = System.Drawing.SystemColors.Control;
            this.txtTvPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTvPath.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.txtTvPath.Location = new System.Drawing.Point(12, 38);
            this.txtTvPath.Name = "txtTvPath";
            this.txtTvPath.ReadOnly = true;
            this.txtTvPath.Size = new System.Drawing.Size(241, 20);
            this.txtTvPath.TabIndex = 1;
            // 
            // btnTvBrowse
            // 
            this.btnTvBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTvBrowse.Location = new System.Drawing.Point(260, 36);
            this.btnTvBrowse.Name = "btnTvBrowse";
            this.btnTvBrowse.Size = new System.Drawing.Size(74, 24);
            this.btnTvBrowse.TabIndex = 2;
            this.btnTvBrowse.Text = "Browse";
            this.btnTvBrowse.UseVisualStyleBackColor = true;
            this.btnTvBrowse.Click += new System.EventHandler(this.btnTvBrowse_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SupportLauncher2.Properties.Resources.tv_bw_48x48_logo_2;
            this.pictureBox1.Location = new System.Drawing.Point(25, 31);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(48, 48);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 511);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCnc);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            this.tabControl1.ResumeLayout(false);
            this.tbpData.ResumeLayout(false);
            this.pnlCsv.ResumeLayout(false);
            this.grpBoxUsers.ResumeLayout(false);
            this.grpBoxUsers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUserSip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUserMob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUserTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUserID)).EndInit();
            this.grpBoxUsersInfo.ResumeLayout(false);
            this.grpBoxUsersInfo.PerformLayout();
            this.grpBoxStores.ResumeLayout(false);
            this.grpBoxStores.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numStoreTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numStoreID)).EndInit();
            this.grpBoxStoreInfo.ResumeLayout(false);
            this.grpBoxStoreInfo.PerformLayout();
            this.grpBoxSource.ResumeLayout(false);
            this.grpBoxSource.PerformLayout();
            this.tbpPrograms.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.grpBoxSkype.ResumeLayout(false);
            this.grpBoxSkype.PerformLayout();
            this.grpBoxSkypePath.ResumeLayout(false);
            this.grpBoxSkypePath.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.grpBoxSQL.ResumeLayout(false);
            this.grpBoxSQL.PerformLayout();
            this.grpBoxSqlPath.ResumeLayout(false);
            this.grpBoxSqlPath.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.grpBoxVNC.ResumeLayout(false);
            this.grpBoxVNC.PerformLayout();
            this.grpBoxVncPath.ResumeLayout(false);
            this.grpBoxVncPath.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.grpBoxTV.ResumeLayout(false);
            this.grpBoxTV.PerformLayout();
            this.grpBoxTvPath.ResumeLayout(false);
            this.grpBoxTvPath.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tabControl1;
        private Button btnOk;
        private Button btnCnc;
        private Button btnApply;
        private TabPage tbpData;
        private GroupBox grpBoxSource;
        private RadioButton rdbDB;
        private RadioButton rdbCsv;
        private Panel pnlCsv;
        private GroupBox grpBoxUsers;
        private GroupBox grpBoxStores;
        private GroupBox grpBoxUsersInfo;
        private GroupBox grpBoxStoreInfo;
        private Label lblStorePW;
        private Label lblStorePath;
        private Label lblUserPW;
        private Label lblUserPath;
        private Label lblStoreTel;
        private Label lblStoreId;
        private Label lblUserSip;
        private Label lblUserMob;
        private Label lblUserTel;
        private Label lblUserId;
        private Button btnStoreBrowse;
        private TextBox txtStorePath;
        private TextBox txtStorePW;
        private CheckBox chkStorePW;
        private NumericUpDown numStoreTel;
        private NumericUpDown numStoreID;
        private CheckBox chkUserPW;
        private Button btnUserBrowse;
        private TextBox txtUserPW;
        private TextBox txtUserPath;
        private NumericUpDown numUserSip;
        private NumericUpDown numUserMob;
        private NumericUpDown numUserTel;
        private NumericUpDown numUserID;
        private TabPage tbpPrograms;
        private Cyotek.Framework.Application.Components.GroupBox grpBoxTV;
        private PictureBox pictureBox1;
        private Cyotek.Framework.Application.Components.GroupBox grpBoxSkype;
        private Cyotek.Framework.Application.Components.GroupBox grpBoxSQL;
        private Cyotek.Framework.Application.Components.GroupBox grpBoxVNC;
        private PictureBox pictureBox2;
        private PictureBox pictureBox3;
        private PictureBox pictureBox4;
        private GroupBox grpBoxTvPath;
        private TextBox txtTvPath;
        private Button btnTvBrowse;
        private GroupBox grpBoxSkypePath;
        private TextBox txtSkypePath;
        private Button btnSkypeBrowse;
        private GroupBox grpBoxSqlPath;
        private TextBox txtSqlPath;
        private Button btnSqlBrowse;
        private GroupBox grpBoxVncPath;
        private TextBox txtVncPath;
        private Button btnVncBrowse;
        private TabPage tabPage1;
        private TextBox textBox2;
        private TextBox textBox1;
        private TextBox textBox3;
        private TextBox textBox4;
        private CheckBox chkDbPW;
        private TextBox txtDbPW;
        private Label lblDbPW;
        private OpenFileDialog openFileDialog1;
    }
}